<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="css/all.min.css">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  
  <!-- For Datatables to use prebuilt search functionality and pagination in PHP -->
  <!-- <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" rel="stylesheet"> -->

  <!-- Custom CSS -->
  <link rel="stylesheet" href="css/style.css">


  <!-- Linking AOS Library -->
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css">

  <title>Online Mobile Repair Service Center</title>
</head>

<body>
  <!-- Start Navigation -->
  <nav class="navbar navbar-expand-sm navbar-dark pl-2 fixed-top" style="background-color: #24ad7f;">
    <a href="index.php" class="navbar-brand" title="Online Mobile Repair Service Center Management System"><i class="fas fa-mobile-alt"></i> Mobile <span>Repair<span></a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#myMenu">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="myMenu">
      <ul class="navbar-nav pl-2 custom-nav">
        <li class="nav-item"><a href="index.php" class="nav-link"><i class="fas fa-home"></i> HOME</a></li>
        <li class="nav-item"><a href="#services" class="nav-link"><i class="fas fa-hands-helping"></i> SERVICE</a></li>
        <li class="nav-item"><a href="#feedback" class="nav-link"><i class="fas fa-comments"></i> REVIEW</a></li>
        <li class="nav-item"><a href="#blog" class="nav-link"><i class="fab fa-blogger-b"></i> BLOG</a></li>
        <li class="nav-item"><a href="#contact" class="nav-link"><i class="fas fa-address-card"></i> CONTACT</a></li>
      </ul>
      <!-- Begin of Button for changing the mode of a webpage -->
      <a class="mode btn" onclick="myFunction()" role="button" title="Change Mode" style="background-color: #28c38e;"><i
            class="fas fa-moon"></i></a>
    <!-- End of Button for changing the mode of a webpage -->
    </div>
  </nav> <!-- End Navigation -->


  <!-- Begin Video Carousel -->
  <section class="video-carousel">
    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <video class="video-fluid" style="opacity: 0.8;" autoplay loop muted>
            <source src="video/carousel1.mp4" type="video/mp4" />

          </video>
          <div class="carousel-caption" style="margin-bottom: 450px;">
            <h1 class="title text-uppercase font-weight-bold text-light">Welcome To <span>Online Mobile Repair</span></h1>
            <p class="font-italic text-light mt-2">Get Your Device Repaired</p>
            <div class="button">
              <a href="user/userLogin.php" class="btn btn-success mr-2 mt-2" title="Login"><i class="fas fa-user-tie"></i> Sign In</a>
              <a href="userRegistration.php" class="btn mt-2" title="Create Your Account" style="background-color: #28c38e;"><i class="fas fa-user-plus"></i> Sign Up</a>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <video class="video-fluid" style="opacity: 0.8;" autoplay loop muted>
            <source src="video/carousel2.mp4" type="video/mp4" type=" video/mp4" />

          </video>
          <div class="carousel-caption" style="margin-bottom: 450px;">
            <h1 class="title text-uppercase font-weight-bold text-light">Welcome To <span>Online Mobile Repair</span></h1>
            <p class="font-italic text-light mt-2">Your Satisfaction is our Objective</p>
            <div class="button">
              <a href="user/userLogin.php" class="btn btn-success mr-2 mt-2" title="Login"><i class="fas fa-user-tie"></i> Sign In</a>
              <a href="userRegistration.php" class="btn mt-2" title="Create Your Account" style="background-color: #28c38e;"><i class="fas fa-user-plus"></i> Sign Up</a>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <video class="video-fluid" style="opacity: 0.8;" autoplay loop muted>
            <source src="video/carousel3.mp4" type="video/mp4" />

          </video>
          <div class="carousel-caption" style="margin-bottom: 450px;">
            <h1 class="title text-uppercase font-weight-bold text-light">Welcome To <span>Online Mobile Repair</span></h1>
            <p class="font-italic text-light mt-2">Your Satisfaction is our Objective</p>
            <div class="button">
              <a href="user/userLogin.php" class="btn btn-success mr-2 mt-2" title="Login"><i class="fas fa-user-tie"></i> Sign In</a>
              <a href="userRegistration.php" class="btn mt-2" title="Create Your Account" style="background-color: #28c38e;"><i class="fas fa-user-plus"></i> Sign Up</a>
            </div>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" title="Previous" role="button"
        data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" title="Next" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </section>
  <!-- End Video Carousel -->

<img class="wave d-print-none" src="images/wave.png">

<!-- Start Topic Info Section -->
  <div class="container" data-aos="fade-up">
    <h2 class="title text-center mt-5">ONLINE <span>REPAIR</span></h2>
    <p class="text-center">
      Online Mobile Repair Services is flourishing in Nepal. It exhibits a great scope and opportunities.
      This application named Online Mobile Repair Application guides you to a trustworthy platform to repair and service your device.
      It includes the troubleshooting the problems raised in the mobile devices. This is the reliable and effective platform to repair your asset.
    </p>
  </div>
<!--Topic Info Section End -->

<!-- Start Services -->
<?php include('indexContent/services.php') ?>
<!-- End Services -->

<!-- Start Feedback Section -->
<?php include('indexContent/feedback.php') ?>
<!-- End Feedback Section -->

<!-- Start Blog Section -->
<?php include('indexContent/blogs.php') ?>
<!-- End Blog Section -->

  <div id="contact" data-aos="zoom-out">
    <!--Start Contact Us-->
    <div class="container">
      <!--Start Contact Us Container-->
      <h2 class="title text-center mb-5">CONTACT <span>US</span></h2> <!-- Contact Us Heading -->
      <div class="row">
        <!--Start Contact Us Row-->
        <div class="col-md-6">
          <!--Start Contact Us 1st Column-->
            <form action="" method="post">
              <input type="text" class="form-control" name="name" placeholder="Name"><br>
              <input type="text" class="form-control" name="subject" placeholder="Subject"><br>
              <input type="email" class="form-control" name="email" placeholder="E-mail"><br>
              <textarea class="form-control" name="message" placeholder="How can we help you?" style="height:150px;"></textarea><br>
              <button type="submit" class="btn mb-2 float-left" name="submit" title="Submit Your Form"><i class="fas fa-paper-plane"></i> Send</button>
              <!-- <input class="btn btn-primary" type="submit" value="Send" name="submit"><br><br> -->
            </form>
        </div> 
        <!-- End Contact Us 1st Column-->

        <!-- Start Contact Us 2nd Column (google Map)-->
        <div class="container googleMap col-md-4 text-center" data-aos="zoom-out">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14131.308868711614!2d85.26189769999999!3d27.69173465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1613446732538!5m2!1sen!2snp" 
        width="425" height="375" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </div>
    </div> <!-- End Contact Us Row-->
  </div> <!-- End Contact Us Container-->
  <!-- End Contact Us -->
</div>

<!-- Start Footer-->
<footer class="text-center text-white" style="background-color: #24ad7f;">
      <!-- Begin Scroll Up Button -->
      <a href="#" class="scrollupbtn btn fixed-bottom" title="Scroll Up" style="background-color: white; border-radius: 50%;"><i class="fas fa-arrow-up"></i></a>
        <!-- End of Scroll Up Button -->
  <!-- Grid container -->
  <div class="container p-4">
    <!-- Section: Social media -->
    <section class="mb-4" data-aos="zoom-in">
      <!-- Facebook -->
      <a class="btn btn-outline-light btn-floating m-1" href="#" target="_blank" title="Join in Facebook with Sulav Adhikari" role="button"
        ><i class="fab fa-facebook-f"></i
      ></a>

      <!-- Twitter -->
      <a class="btn btn-outline-light btn-floating m-1" href="#" target="_blank" title="Twitter" role="button"
        ><i class="fab fa-twitter"></i
      ></a>

      <!-- Google -->
      <a class="btn btn-outline-light btn-floating m-1" href="#" target="_blank" title="Google" role="button"
        ><i class="fab fa-google"></i
      ></a>

      <!-- Instagram -->
      <a class="btn btn-outline-light btn-floating m-1" href="#" target="_blank" title="Instagram" role="button"
        ><i class="fab fa-instagram"></i
      ></a>

      <!-- Linkedin -->
      <a class="btn btn-outline-light btn-floating m-1" href="#" target="_blank" title="LinkedIn" role="button"
        ><i class="fab fa-linkedin-in"></i
      ></a>

      <!-- Github -->
      <a class="btn btn-outline-light btn-floating m-1" href="#" target="_blank" title="GitHub" role="button"
        ><i class="fab fa-github"></i
      ></a>
    </section>
    <!-- Section: Social media -->

    <!-- Section: Text -->
    <section class="mb-4">
      <p>
      Online Mobile Repair Application guides you to a trustworthy platform to repair and service your device.
      </p>
    </section>
    <!-- Section: Text -->

    <!-- Section: Links -->
    <section class="">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="title col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase text-white font-weight-bold float-left"><span>Web </span>Pages</h5>
        <br>
          <ul class="list-unstyled mb-0">
            <br>
            <li>
              <a href="index.php" class="text-white float-left"><i class="fas fa-home"></i> Home</a>
            </li>
            <br>
            <li>
              <a href="#services" class="text-white float-left"><i class="fas fa-hands-helping"></i> Service</a>
            </li>
            <br>
            <li>
              <a href="#feedback" class="text-white float-left"><i class="fas fa-comments"></i> Feedback</a>
            </li>
            <br>
            <li>
              <a href="#blog" class="text-white float-left"><i class="fab fa-blogger-b"></i> Blog</a>
            </li>
            <br>
            <li>
              <a href="#contact" class="text-white float-left"><i class="fas fa-address-card"></i> Contact</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="title col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase text-white font-weight-bold float-left"><span>Cont</span>act</h5>
          <br>
          <ul class="list-unstyled mb-0">
          <br>
            <li>
              <a href="#!" class="text-white float-left"><i class="fas fa-map-marker-alt"></i> Kathmandu, Nepal</a>
            </li>
            <br>
            <li>
              <a href="#!" class="text-white float-left"><i class="fas fa-envelope"></i> adhikarisulav81@gmail.com</a>
            </li>
            <br>
            <li>
              <a href="#!" class="text-white float-left"><i class="fas fa-phone"></i> 1234145345</a>
            </li>

          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="title col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase text-white font-weight-bold float-left"><span>Legal </span>Policies</h5>
        <br>
          <ul class="list-unstyled mb-0">
          <br>
            <li>
              <a href="#!" class="text-white float-left">Terms and Conditions</a>
            </li>
            <br>
            <li>
              <a href="#!" class="text-white float-left">Privacy Policiy</a>
            </li>
            <br>
            

          </ul>
        </div>
        <!--Grid column-->


        <!--Grid column-->
        <div class="title col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase text-white font-weight-bold float-left"><span>Quick </span>Info</h5>
        <br>
          <ul class="list-unstyled mb-0">
          <br>
            <li>
              <a href="#!" class="text-white float-left">About Online Mobile Repair</a>
            </li>
            <br>
            <li>
            <a href="" class="text-white float-left">How It Works ?</a>
            </li>
            <br>
            <li>
            <a href="indexContent/frequentlyAsked.php" class="text-white float-left">FAQs</a>
            </li>

          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </section>
    <!-- Section: Links -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2021 Copyright:
    <a class="text-white" href="#">Online Mobile Repair Service</a>

  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->
<!-- End Footer -->


<!-- Importing AOS library to apply animation to the content while scrolling. Like in feeback section, the cards animate as up and down because of this library -->
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init({
      //disable: 'mobile',    //this disables the scrolling animation for mobile screens
      offset: 350,
      duration: 2000
    });
  </script>
  <!-- End of Importing library for the animation -->

  <!-- Boostrap JavaScript -->
  <script src="http://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/all.min.js"></script>

  <!-- JS for Mode in a Webpage (Dark or light mode) -->
  <script src="js/custom.js"></script>
  
</body>

</html>