<?php
define('TITLE', 'Requests');
define('PAGE', 'request');
include('includes/header.php'); 
include('../dbConnection.php');
session_start();

 if(isset($_SESSION['is_adminlogin'])){
  $aEmail = $_SESSION['aEmail'];
 } else {
  echo "<script> location.href='adminLogin.php'; </script>";
 }
?>

<img class="wave" src="../images/wave.png">

<div class="col-sm-5 mt-5">
  <h3 class="text-center font-weight-bold text-dark" style="font-family: Arial, Helvetica, sans-serif;"><i class="fas fa-people-carry"></i> REQUESTS</h3>
  <!-- Main Content area start Middle -->
  <?php 
    $sql = "SELECT request_id, request_info, request_desc, request_date FROM submitrequest_tb";
    $result = $conn->query($sql);

    if($result->num_rows > 0){
        echo '<div class="table-responsive-sm">';
          echo '<table id="dataTableID" class="table">
          <thead>
            <tr>
              <th scope="col">Req ID</th>
              <th scope="col">Info</th>
              
              <th scope="col">Date</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>';
            while($row = $result->fetch_assoc()){
              echo '<tr>
              <th scope="row">'.$row["request_id"].'.'.'</th>
              <td>'.$row["request_info"].'</td>
              
              <td>'.$row["request_date"].'</td>
              <td>
                <form action="" method="POST" class="d-inline"> 
                  <input type="hidden" name="id" value='. $row["request_id"] .'>
                  <button type="submit" class="btn btn-sm mb-2" name="view"><i class="far fa-eye"></i></button>
                </form>
                <form action="" method="POST" class="d-inline">
                  <input type="hidden" name="id" value='. $row["request_id"] .'>
                  <button type="submit" class="btn btn-danger mb-2" name="close"><i class="far fa-trash-alt"></i></button>
                </form>
              </td>
              </tr>';
            }
          echo '</tbody> </table>';
        echo '</div>';
      } else {
        echo '<div class="alert alert-info mt-5 col-sm-6" role="alert">
                <h4 class="alert-heading">Well done!</h4>
                <p>Aww yeah, you successfully assigned all Requests.</p>
                <hr>
                <h5 class="mb-0">No Pending Requests</h5>
            </div>';
        }

// after assigning work we will delete data from submitrequesttable by pressing close button
if(isset($_REQUEST['close'])){
  $sql = "DELETE FROM submitrequest_tb WHERE request_id = {$_REQUEST['id']}";
  if($conn->query($sql) === TRUE){
    // echo "Record Deleted Successfully";
    // below code will refresh the page after deleting the record
    echo '<meta http-equiv="refresh" content= "0;URL=?closed" />';
    } else {
      echo "Unable to Delete Data";
    }
  }
 ?>
 
</div> <!-- Main Content area End Middle -->

<?php 
  include('includes/footer.php');
  include('assignworkform.php');
  $conn->close();
?>