<?php
define('TITLE', 'Assigned Work');
define('PAGE', 'viewassignwork');
include('includes/header.php'); 
include('../dbConnection.php');
session_start();

 if(isset($_SESSION['is_adminlogin'])){
  $aEmail = $_SESSION['aEmail'];
 } else {
  echo "<script> location.href='adminLogin.php'; </script>";
 }
?>

<img class="wave d-print-none" src="../images/wave.png">

<div class="col-sm-6 ">
    <h3 class="title text-center font-weight-bold text-dark mt-5" style="font-family: Arial, Helvetica, sans-serif  ;">
    <i class="fas fa-briefcase"></i> ASSIGNED <span>WORK DETAILS</span></h3>

 <?php
 if(isset($_REQUEST['view'])){
    $sql = "SELECT * FROM assignwork_tb WHERE request_id = {$_REQUEST['id']}";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
 }
 ?>
<div class="table-responsive-sm">
   <table class="table table-bordered">
      <tbody>
         <tr>
         <td>Request ID</td>
         <td>
         <?php if(isset($row['request_id'])) {echo $row['request_id']; }?>
         </td>
         </tr>
         <tr>
         <td>Request Info</td>
         <td>
         <?php if(isset($row['request_info'])) {echo $row['request_info']; }?>
         </td>
         </tr>
         <tr>
         <td>Request Description</td>
         <td>
         <?php if(isset($row['request_desc'])) {echo $row['request_desc']; }?>
         </td>
         </tr>
         <tr>
         <td>Name</td>
         <td>
         <?php if(isset($row['requester_name'])) {echo $row['requester_name']; }?>
         </td>
         </tr>
         <tr>
         <td>Address Line 1</td>
         <td>
         <?php if(isset($row['requester_add1'])) {echo $row['requester_add1']; }?>
         </td>
         </tr>
         <tr>
         <td>Address Line 2</td>
         <td>
         <?php if(isset($row['requester_add2'])) {echo $row['requester_add2']; }?>
         </td>
         </tr>
         <tr>
         <td>City</td>
         <td>
         <?php if(isset($row['requester_city'])) {echo $row['requester_city']; }?>
         </td>
         </tr>
         <tr>
         <td>State</td>
         <td>
         <?php if(isset($row['requester_state'])) {echo $row['requester_state']; }?>
         </td>
         </tr>
         <tr>
         <td>Pin Code</td>
         <td>
         <?php if(isset($row['requester_zip'])) {echo $row['requester_zip']; }?>
         </td>
         </tr>
         <tr>
         <td>Email</td>
         <td>
         <?php if(isset($row['requester_email'])) {echo $row['requester_email']; }?>
         </td>
         </tr>
         <tr>
         <td>Mobile</td>
         <td>
         <?php if(isset($row['requester_mobile'])) {echo $row['requester_mobile']; }?>
         </td>
         </tr>
         <tr>
         <td>Assigned Date</td>
         <td>
         <?php if(isset($row['assign_date'])) {echo $row['assign_date']; }?>
         </td>
         </tr>
         <tr>
         <td>Technician Name</td>
         <td>
         <?php if(isset($row['assign_tech'])) {echo $row['assign_tech']; }?>
         </td>
         </tr>
         <tr>
         <td>Device Status</td>
         <td>
         <?php if(isset($row['status'])) {echo $row['status']; }?>
         </td>
         </tr>
         <tr>
         <td>Delivery Date</td>
         <td>
         <?php if(isset($row['deliveryDate'])) {echo $row['deliveryDate']; }?>
         </td>
         </tr>
         <tr>
         <td>Customer Sign</td>
         <td></td>
         </tr>
         <tr>
         <td>Technician Sign</td>
         <td></td>
         </tr>
      </tbody>
   </table>
 </div>

 <div class="text-center">
    <button type="submit" class="btn btn-info d-print-none" onClick='window.print()'><i class="fas fa-print"></i> Print</button>
    <a href="work.php" type="submit" class="btn btn-secondary d-print-none"><i class="far fa-times-circle"></i> Close</a>
 </div>
</div>

<?php
include('includes/footer.php'); 
?>