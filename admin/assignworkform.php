<?php    
/*
  since in request page the session is started and again in this also started ..
  so to maintain this issues as this page is included in request page session id is introduced.
*/
if(session_id() == '') { 
  session_start();
}
if(isset($_SESSION['is_adminlogin'])){  
 $aEmail = $_SESSION['aEmail'];
} else {
 echo "<script> location.href='adminLogin.php'; </script>";
}
 if(isset($_REQUEST['view'])){
  $sql = "SELECT * FROM submitrequest_tb WHERE request_id = {$_REQUEST['id']}";
 $result = $conn->query($sql);
 $row = $result->fetch_assoc();
 }

 //  Assign work Order Request Data going to submit and save on db assignwork_tb table
 if(isset($_REQUEST['assign'])){
  // Checking for Empty Fields
  if(($_REQUEST['request_id'] == "") || ($_REQUEST['request_info'] == "") || ($_REQUEST['requestdesc'] == "") || ($_REQUEST['requestername'] == "")
   || ($_REQUEST['address1'] == "") || ($_REQUEST['address2'] == "") || ($_REQUEST['requestercity'] == "") || ($_REQUEST['requesterstate'] == "") 
   || ($_REQUEST['requesterzip'] == "") || ($_REQUEST['requesteremail'] == "") || ($_REQUEST['requestermobile'] == "") || 
   ($_REQUEST['assigntech'] == "") || ($_REQUEST['inputdate'] == "")){
   // msg displayed if required field missing
   $msg = '<div class="alert alert-warning col-sm-6 mt-2" role="alert"> Fill All Fileds </div>';
  } else {
    // Assigning User Values to Variable
    $rid = $_REQUEST['request_id'];
    $rinfo = $_REQUEST['request_info'];
    $rdesc = $_REQUEST['requestdesc'];
    $rname = $_REQUEST['requestername'];
    $radd1 = $_REQUEST['address1'];
    $radd2 = $_REQUEST['address2'];
    $rcity = $_REQUEST['requestercity'];
    $rstate = $_REQUEST['requesterstate'];
    $rzip = $_REQUEST['requesterzip'];
    $remail = $_REQUEST['requesteremail'];
    $rmobile = $_REQUEST['requestermobile'];
    $rassigntech = $_REQUEST['assigntech'];
    $rdate = $_REQUEST['inputdate'];
    $status = $_REQUEST['status'];
    $ddate = $_REQUEST['deliveryDate'];

    $sql = "INSERT INTO assignwork_tb (request_id, request_info, request_desc, requester_name, requester_add1, requester_add2, requester_city, 
    requester_state, requester_zip, requester_email, requester_mobile, assign_tech, assign_date, status, deliveryDate) 
    VALUES ('$rid', '$rinfo','$rdesc', '$rname', '$radd1', '$radd2', '$rcity', '$rstate', '$rzip', '$remail', 
    '$rmobile', '$rassigntech', '$rdate', '$status', '$ddate')";
    if($conn->query($sql) == TRUE){
     // below msg display on form submit success
     $msg = '<div class="alert alert-success col-sm-6 mt-2" role="alert"> Work Assigned Successfully </div>';
    } else {
     // below msg display on form submit failed
     $msg = '<div class="alert alert-danger col-sm-6 mt-2" role="alert"> Unable to Assign Work </div>';
    }
  }
}
?>

<img class="wave" src="../images/wave.png">

<div class="col-sm-5 mt-5">
  <h3 class="title text-center font-weight-bold text-dark" style="font-family: Arial, Helvetica, sans-serif;">
  <i class="fas fa-briefcase"></i> ASSIGN <span>WORK FORM</span></h3>
  <!-- Main Content area Start Last -->
  <form action="" method="POST">
    <div class="form-group">
    <i class="fas fa-id-card-alt"></i>
      <label for="request_id"> Request ID</label>
      <input type="text" class="form-control" id="request_id" name="request_id" value="<?php if(isset($row['request_id'])) {echo $row['request_id']; }?>"
        readonly>
    </div>
    <div class="form-group">
    <i class="fas fa-info-circle"></i>
      <label for="request_info"> Request Info</label>
      <input type="text" class="form-control" id="request_info" name="request_info" value="<?php if(isset($row['request_info'])) {echo $row['request_info']; }?>">
    </div>
    <div class="form-group">
      <label for="requestdesc"><i class="fas fa-pen-nib"></i> Description</label>
      <input type="text" class="form-control" id="requestdesc" name="requestdesc" value="<?php if(isset($row['request_desc'])) { echo $row['request_desc']; } ?>">
    </div>
    <div class="form-group">
      <label for="requestername"><i class="fas fa-users"></i> Name</label>
      <input type="text" class="form-control" id="requestername" name="requestername" value="<?php if(isset($row['requester_name'])) { echo $row['requester_name']; } ?>">
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="address1"><i class="fas fa-map-marker-alt"></i> Temporary Address</label>
        <input type="text" class="form-control" id="address1" name="address1" value="<?php if(isset($row['requester_add1'])) { echo $row['requester_add1']; } ?>">
      </div>
      <div class="form-group col-md-6">
        <label for="address2"><i class="fas fa-map-marker-alt"></i> Permanent Address</label>
        <input type="text" class="form-control" id="address2" name="address2" value="<?php if(isset($row['requester_add2'])) {echo $row['requester_add2']; }?>">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="requestercity"><i class="fas fa-city"></i> City</label>
        <input type="text" class="form-control" id="requestercity" name="requestercity" value="<?php if(isset($row['requester_city'])) {echo $row['requester_city']; }?>">
      </div>
      <div class="form-group col-md-4">
        <label for="requesterstate"><i class="fas fa-flag"></i> State</label>
        <input type="text" class="form-control" id="requesterstate" name="requesterstate" value="<?php if(isset($row['requester_state'])) { echo $row['requester_state']; } ?>">
      </div>
      <div class="form-group col-md-4">
        <label for="requesterzip"><i class="fas fa-globe-asia"></i> Zip</label>
        <input type="text" class="form-control" id="requesterzip" name="requesterzip" value="<?php if(isset($row['requester_zip'])) { echo $row['requester_zip']; } ?>"
          onkeypress="isInputNumber(event)">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-8">
        <label for="requesteremail"><i class="far fa-envelope"></i> Email</label>
        <input type="email" class="form-control" id="requesteremail" name="requesteremail" value="<?php if(isset($row['requester_email'])) {echo $row['requester_email']; }?>">
      </div>
      <div class="form-group col-md-4">
        <label for="requestermobile"><i class="fas fa-mobile"></i> Mobile</label>
        <input type="text" class="form-control" id="requestermobile" name="requestermobile" value="<?php if(isset($row['requester_mobile'])) {echo $row['requester_mobile']; }?>"
          onkeypress="isInputNumber(event)">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="assigntech"><i class="fas fa-chalkboard-teacher"></i> Assign to Technician</label>
        <input type="text" class="form-control" id="assigntech" name="assigntech">
      </div>
      <div class="form-group col-md-6">
        <label for="inputDate"><i class="fas fa-calendar-alt"></i> Assigned Date</label>
        <input type="text" class="form-control" id="inputDate" name="inputdate">
      </div>
      <div class="form-group col-md-6">
        <label for="status"><i class="fas fa-calendar-alt"></i> Device Status</label>
        <input type="text" class="form-control" id="status" name="status">
      </div>
      <div class="form-group col-md-6">
        <label for="deliveryDate"><i class="fas fa-calendar-alt"></i> Delivery Date</label>
        <input type="text" class="form-control" id="deliveryDate" name="deliveryDate">
      </div>
    </div>
    <div class="float-right">
      <button type="submit" class="btn btn-success" name="assign"><i class="fab fa-google-drive"></i> Assign</button>
      <button type="reset" class="btn btn-secondary"><i class="fas fa-cut"></i> Reset</button>
    </div>
  </form>
  <!-- start datepicker implementation for date validation in assign date and delivery date field -->
        <script>
            $(document).ready(function () {


                $("#inputDate").datepicker({
                    showAnim: 'drop',
                    numberOfMonth: 1,
                    dateFormat: 'yy-mm-dd',
                    onClose: function (selectedDate) {
                        $("#deliveryDate").datepicker("option", "minDate", selectedDate);
                    }
                });

                $("#deliveryDate").datepicker({
                    showAnim: 'drop',
                    numberOfMonth: 1,
                    dateFormat: 'yy-mm-dd',
                    onClose: function (selectedDate) {
                        $("#inputDate").datepicker("option", "maxDate", selectedDate);
                    }
                });
            });
        </script>
  <!-- end datepicker implementation for date validation in assign date and delivery date field -->

  <!-- below msg display if required fill missing or form submitted success or failed -->
  <?php if(isset($msg)) {echo $msg; } ?>
  </div> <!-- Main Content area End Last -->
</div> <!-- End Row -->
</div> <!-- End Container -->
<!-- Only Number for input fields -->
<script>
  function isInputNumber(evt) {
    var ch = String.fromCharCode(evt.which);
    if (!(/[0-9]/.test(ch))) {
      evt.preventDefault();
    }
  }
</script>