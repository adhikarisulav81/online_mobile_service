<?php
define('TITLE', 'Assigned Work');
define('PAGE', 'work');
include('includes/header.php'); 
include('../dbConnection.php');
session_start();

 if(isset($_SESSION['is_adminlogin'])){
  $aEmail = $_SESSION['aEmail'];
 } else {
  echo "<script> location.href='adminLogin.php'; </script>";
 }
?>

<img class="wave" src="../images/wave.png">

<div class="col-sm-9 col-md-10">
  <h3 class="title text-center font-weight-bold text-dark mb-5 mt-5" style="font-family: Arial, Helvetica, sans-serif;">
  <i class="fas fa-briefcase"></i> ASSIGNED <span>WORK</span></h3>

  <?php 
    $sql = "SELECT * FROM assignwork_tb";
    $result = $conn->query($sql);
  if($result->num_rows > 0){
    echo '<div class="table-responsive-sm">';
      echo '<table id="dataTableID" class="table">
      <thead>
        <tr>
          <th scope="col">Req ID</th>
          <th scope="col">Request Info</th>
          <th scope="col">Name</th>
          <th scope="col">Address</th>
          <th scope="col">City</th>
          <th scope="col">Mobile</th>
          <th scope="col">Technician</th>
          <th scope="col">Assigned Date</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>';
        while($row = $result->fetch_assoc()){
          echo '<tr>
          <th scope="row">'.$row["request_id"].'.'.'</th>
          <td>'.$row["request_info"].'</td>
          <td>'.$row["requester_name"].'</td>
          <td>'.$row["requester_add2"].'</td>
          <td>'.$row["requester_city"].'</td>
          <td>'.$row["requester_mobile"].'</td>
          <td>'.$row["assign_tech"].'</td>
          <td>'.$row["assign_date"].'</td>
          <td>
            <form action="viewassignwork.php" method="POST" class="d-inline"> 
              <input type="hidden" name="id" value='. $row["request_id"] .'>
              <button type="submit" class="btn mr-2 mb-2" name="view" value="View"><i class="far fa-eye"></i></button>
            </form>
            <form action="" method="POST" class="d-inline">
              <input type="hidden" name="id" value='. $row["request_id"] .'>
              <button type="submit" class="btn btn-danger mb-2" name="delete" value="Delete"><i class="far fa-trash-alt"></i></button>
            </form>
          </td>
          </tr>';
        }
      echo '</tbody> </table>';
    echo '</div>';
  } else {
    echo "No Records Found.";
  }
  if(isset($_REQUEST['delete'])){
    $sql = "DELETE FROM assignwork_tb WHERE request_id = {$_REQUEST['id']}";
    if($conn->query($sql) === TRUE){
      // below code will refresh the page after deleting the record
      echo '<meta http-equiv="refresh" content= "0;URL=?deleted" />';
      } else {
        echo "Unable to Delete Data";
      }
    }
  ?>

  </div>
</div>
</div>

<?php
include('includes/footer.php'); 
?>