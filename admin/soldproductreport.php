<?php
define('TITLE', 'Sold Product Report');
define('PAGE', 'soldproductreport');
include('includes/header.php');
include('../dbConnection.php');
session_start();
 if(isset($_SESSION['is_adminlogin'])){
  $aEmail = $_SESSION['aEmail'];
 } else {
  echo "<script> location.href='adminLogin.php'; </script>";
 } 
?>
<img class="wave" src="../images/wave.png">

<div class="col-sm-9 col-md-10 mt-5 text-center">
<h3 class="title text-center font-weight-bold mb-5" style="font-family: Arial, Helvetica, sans-serif;"><i class="fas fa-clipboard"></i> SOLD PRODUCT <span>REPORT</span></h3>

  <form action="" method="POST" class="d-print-none">
    <div class="form-row">
      <div class="form-group col-md-2">
        <input type="date" class="form-control" id="startdate" name="startdate">
      </div> <span> to </span>
      <div class="form-group col-md-2">
        <input type="date" class="form-control" id="enddate" name="enddate">
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-info" name="searchsubmit" value="Search">
      </div>
    </div>
  </form>
  <?php
 if(isset($_REQUEST['searchsubmit'])){
    $startdate = $_REQUEST['startdate'];
    $enddate = $_REQUEST['enddate'];
    // $sql = "SELECT * FROM customer_tb WHERE cpdate BETWEEN '2018-10-11' AND '2018-10-13'";
    $sql = "SELECT * FROM customer_tb WHERE cpdate BETWEEN '$startdate' AND '$enddate'";
    $result = $conn->query($sql);
    if($result->num_rows > 0){
     echo '
     <div class="table-responsive-sm">
  <h3 class="title text-center font-weight-bold text-dark mb-5 mt-5" style="font-family: Arial, Helvetica, sans-serif;">
  <i class="fas fa-clipboard"></i> REPORT <span>DETAILS</span></h3>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Customer ID</th>
        <th scope="col">Customer Name</th>
        <th scope="col">Address</th>
        <th scope="col">Product Name</th>
        <th scope="col">Quantity</th>
        <th scope="col">Price Each</th>
        <th scope="col">Total</th>
        <th scope="col">Date</th>
      </tr>
    </thead>
    <tbody>';
    while($row = $result->fetch_assoc()){
      echo '<tr>
        <th scope="row">'.$row["custid"].'</th>
        <td>'.$row["custname"].'</td>
        <td>'.$row["custadd"].'</td>
        <td>'.$row["cpname"].'</td>
        <td>'.$row["cpquantity"].'</td>
        <td>'.$row["cpeach"].'</td>
        <td>'.$row["cptotal"].'</td>
        <td>'.$row["cpdate"].'</td>
      </tr>';
    }
    echo '<tr>
      <td>
        <form class="d-print-none">
          <button type="submit" class="btn btn-info mt-5" name="Print" onClick="window.print()""><i class="fas fa-print"></i> Print</button>
        </form>
      </td>
      <td>
        <form class="d-print-none" action="soldproductreport.php">
          <button type="submit" class="btn btn-secondary mr-5 mt-5" name="Back""><i class="fas fa-backward"></i> Back</button>
        </form>
      </td>
    </tr>
    </tbody>
  </table>';
  } else {
    echo "<div class='alert alert-warning col-sm-6 mt-2' role='alert'> No Records Found !</div>";
  }
 }
  ?>
</div>
</div>
</div>

<?php
include('includes/footer.php'); 
?>