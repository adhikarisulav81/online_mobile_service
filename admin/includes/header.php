<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta http-equiv="X-UA-Compatible" content="ie=edge">
 <title>
  <?php echo TITLE ?>
 </title>
 <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="../css/bootstrap.min.css">

 <!-- Font Awesome CSS -->
 <link rel="stylesheet" href="../css/all.min.css">

<!-- For Datatables to use prebuilt search functionality and pagination in PHP -->
<link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" rel="stylesheet">

 <!-- Custome CSS -->
 <link rel="stylesheet" href="../css/style.css">

</head>

<body>
 <!-- Top Navbar -->
 <nav class="navbar navbar-dark fixed-top p-0 shadow" style="background-color: #24ad7f;">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="dashboard.php" title="Online Mobile Repair Service Center Management System"><i class="fas fa-mobile"></i> Online Repair</a>
 </nav>

 <!-- Side Bar -->
 <div class="container-fluid mb-5" style="margin-top:40px;">
  <div class="row">
   <nav class="col-sm-3 col-md-2 sidebar py-5 d-print-none">
    <div class="sidebar-sticky">
     <ul class="nav flex-column">
      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'dashboard') { echo 'active'; } ?> " href="dashboard.php">
        <i class="fas fa-tachometer-alt"></i>
        Dashboard
       </a>
      </li>
      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'work') { echo 'active'; } ?>" href="work.php">
       <i class="fas fa-briefcase"></i>
        Work Order
       </a>
      </li>

      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'request') { echo 'active'; } ?>" href="request.php">
       <i class="fas fa-people-carry"></i>
        Request-Assign
       </a>
      </li>

      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'assets') { echo 'active'; } ?>" href="assets.php">
       <i class="fas fa-shopping-cart"></i>
        Assets
       </a>
      </li>

      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'soldproductreport') { echo 'active'; } ?>" href="soldproductreport.php">
       <i class="fas fa-clipboard"></i>
       Sell Report
       </a>
      </li>

      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'workreport') { echo 'active'; } ?>" href="workreport.php">
       <i class="fas fa-clipboard"></i>
        Work Report
       </a>
      </li>

      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'technician') { echo 'active'; } ?>" href="technician.php">
       <i class="fas fa-chalkboard-teacher"></i>
        Technician
       </a>
      </li>

      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'viewadmin') { echo 'active'; } ?>" href="viewadmin.php">
       <i class="fas fa-user-cog"></i>
        Admin
       </a>
      </li>
      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'user') { echo 'active'; } ?>" href="user.php">
        <i class="fas fa-users"></i>
        Users
       </a>
      </li>

      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'changepass') { echo 'active'; } ?>" href="changepass.php">
       <i class="fas fa-lock"></i>
        Change Password
       </a>
      </li>
      <li class="nav-item">
       <a class="nav-link" href="../logout.php">
        <i class="fas fa-sign-out-alt"></i>
        Logout
       </a>
      </li>
     </ul>
    </div>
   </nav>