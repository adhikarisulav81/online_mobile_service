<?php
define('TITLE', 'Dashboard');
define('PAGE', 'dashboard');
include('includes/header.php'); 
include('../dbConnection.php');
session_start();

 if(isset($_SESSION['is_adminlogin'])){
  $aEmail = $_SESSION['aEmail'];
 } else {
  echo "<script> location.href='adminLogin.php'; </script>";
 }
  // $sql = "SELECT max(request_id) FROM submitrequest_tb";
  // $result = $conn->query($sql);
  // $row = mysqli_fetch_row($result);
  // $submitrequest = $row[0];

  $sql = "SELECT * FROM submitrequest_tb";
  $result = $conn->query($sql);
  $submitrequest = $result->num_rows;

  $sql = "SELECT * FROM assignwork_tb";
  $result = $conn->query($sql);
  $assignwork = $result->num_rows;

  $sql = "SELECT * FROM technician_tb";
  $result = $conn->query($sql);
  $totaltech = $result->num_rows;

  $sql = "SELECT * FROM assets_tb";
  $result = $conn->query($sql);
  $totalassets = $result->num_rows;

  $sql = "SELECT * FROM userlogin_tb";
  $result = $conn->query($sql);
  $totaluser = $result->num_rows;

  $sql = "SELECT * FROM adminlogin_tb";
  $result = $conn->query($sql);
  $totaladmin = $result->num_rows;
?>

<img class="wave" src="../images/wave.png">

<div class="col-sm-9 col-md-10">
  <h3 class="title text-center font-weight-bold text-dark mb-5 mt-5" style="font-family: Arial, Helvetica, sans-serif;"><i class="fas fa-tachometer-alt"></i> ADMIN <span>DASHBOARD</span></h3>
  <div class="row mx-5 text-center">
    <div class="col-sm-4 mt-5">
      <div class="card mb-3" style="max-width: 18rem; background-color: #E27D60;">
        <div class="card-header"><i class="fas fa-people-carry"></i> Request Received</div>
          <div class="card-body">
            <h4 class="card-title">
              <?php echo $submitrequest; ?>
            </h4>
            <a href="request.php" class="btn" title="Glance" style="background-color: white;"><i class="far fa-eye"></i> View</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mt-5">
        <div class="card mb-3" style="max-width: 18rem; background-color: #BC986A;">
          <div class="card-header"><i class="fas fa-briefcase"></i> Assigned Task</div>
          <div class="card-body">
            <h4 class="card-title">
              <?php echo $assignwork; ?>
            </h4>
            <a href="work.php" class="btn" title="Glance" style="background-color: white;"><i class="far fa-eye"></i> View</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mt-5">
        <div class="card mb-3" style="max-width: 18rem; background-color: #9BBC46;">
          <div class="card-header"><i class="fas fa-chalkboard-teacher"></i> No. of Technician</div>
            <div class="card-body">
              <h4 class="card-title">
                <?php echo $totaltech; ?>
              </h4>
              <a href="technician.php" class="btn" title="Glance" style="background-color: white;"><i class="far fa-eye"></i> View</a>
            </div>
          </div>
        </div>
      <div class="col-sm-4 mt-5">
        <div class="card mb-3" style="max-width: 18rem; background-color: #2E8B57;">
          <div class="card-header"><i class="fas fa-shopping-cart"></i> Assets</div>
          <div class="card-body">
            <h4 class="card-title">
              <?php echo $totalassets; ?>
            </h4>
            <a href="assets.php" class="btn" title="Glance" style="background-color: white;"><i class="far fa-eye"></i> View</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mt-5">
        <div class="card mb-3" style="max-width: 18rem; background-color: #28c38e;">
          <div class="card-header"><i class="fas fa-users"></i> Users</div>
          <div class="card-body">
            <h4 class="card-title">
              <?php echo $totaluser; ?>
            </h4>
            <a href="user.php" class="btn" title="Glance" style="background-color: white;"><i class="far fa-eye"></i> View</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 mt-5">
        <div class="card mb-3" style="max-width: 18rem; background-color: #767E7A;">
          <div class="card-header"><i class="fas fa-user-cog"></i> Admin</div>
          <div class="card-body">
            <h4 class="card-title">
              <?php echo $totaladmin; ?>
            </h4>
            <a href="viewadmin.php" class="btn" title="Glance" style="background-color: white;"><i class="far fa-eye"></i> View</a>
          </div>
        </div>
      </div>
      
      </div>
      
      <!--<div class="mx-5 mt-5"> -->
        <!--Table-->
        <!-- <p class="p-2 mb-3 text-center" style="font-size: 1.2rem; font-weight: bold;"><i class="fas fa-users"></i> List of Users</p> -->
        <?php
          // $sql = "SELECT * FROM userlogin_tb";
          // $result = $conn->query($sql);
          // if($result->num_rows > 0){
          //   echo '<div class="table-responsive-sm">';
          //   echo '<table id="dataTableID" class="table">
          //     <thead>
          //     <tr>
          //       <th scope="col">User ID</th>
          //       <th scope="col">Name</th>
          //       <th scope="col">Email</th>
          //     </tr>
          //     </thead>
          //     <tbody>';
          //     while($row = $result->fetch_assoc()){
          //     echo '<tr>';
          //       echo '<th scope="row">'.$row["r_login_id"].'.'.'</th>';
          //       echo '<td>'. $row["r_name"].'</td>';
          //       echo '<td>'.$row["r_email"].'</td>';
          //     }
          //   echo '</tbody>
          //   </table>';
          //   echo '</div>';
          //   } else {
          //     echo "0 Result";
          //   }
        ?>
      </div>
    </div>
  </div>
</div>

<?php
include('includes/footer.php'); 
?>