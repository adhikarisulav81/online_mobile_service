<?php
define('TITLE', 'Technician');
define('PAGE', 'technician');
include('includes/header.php'); 
include('../dbConnection.php');
session_start();

 if(isset($_SESSION['is_adminlogin'])){
  $aEmail = $_SESSION['aEmail'];
 } else {
  echo "<script> location.href='adminLogin.php'; </script>";
 }
?>

<img class="wave" src="../images/wave.png">

<div class="col-sm-9 col-md-10">
  <!--Table-->
  <h3 class="title text-center font-weight-bold text-dark mb-5 mt-5" style="font-family: Arial, Helvetica, sans-serif;"><i class="fas fa-chalkboard-teacher"></i> LIST OF <span>TECHNICIAN</span></h3>
  <?php
    $sql = "SELECT * FROM technician_tb";
    $result = $conn->query($sql);
    if($result->num_rows > 0){
      echo '<div class="table-responsive-sm">';
      echo '<table id="dataTableID" class="table">
        <thead>
        <tr>
          <th scope="col">Emp ID</th>
          <th scope="col">Name</th>
          <th scope="col">City</th>
          <th scope="col">Mobile</th>
          <th scope="col">Email</th>
          <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>';
        while($row = $result->fetch_assoc()){
          echo '<tr>';
            echo '<th scope="row">'.$row["empid"].'.'.'</th>';
            echo '<td>'. $row["empName"].'</td>';
            echo '<td>'.$row["empCity"].'</td>';
            echo '<td>'.$row["empMobile"].'</td>';
            echo '<td>'.$row["empEmail"].'</td>';
            echo '<td>
              <form action="editemp.php" method="POST" class="d-inline"> 
                <input type="hidden" name="id" value='. $row["empid"] .'>
                <button type="submit" class="btn mr-2 mb-2" name="view" value="View"><i class="fas fa-eye"></i></button>
              </form>  
              <form action="" method="POST" class="d-inline">
                <input type="hidden" name="id" value='. $row["empid"] .'>
                <button type="submit" class="btn btn-danger mr-2 mb-2" name="delete" value="Delete"><i class="far fa-trash-alt"></i></button>
              </form>
            </td>
          </tr>';
    }

        echo '</tbody>
      </table>';
      echo '</div>';
  } else {
    echo "No Records Found.";
  }
if(isset($_REQUEST['delete'])){
  $sql = "DELETE FROM technician_tb WHERE empid = {$_REQUEST['id']}";
  if($conn->query($sql) === TRUE){
    // below code will refresh the page after deleting the record
    echo '<meta http-equiv="refresh" content= "0;URL=?deleted" />';
    } else {
      echo "Unable to Delete Data";
    }
  }
?>

  </div>
</div>
<div class="text-center">
  <a href="insertemp.php" class="btn fixed-bottom" title="Add Technician" style="background-color: #28c38e;"><i class="fas fa-plus fa"></i> Add New Technician</a>

  <!-- <a class="btn btn-danger box" href="insertemp.php"><i class="fas fa-plus fa-2x"></i></a> -->
  </div>
</div>

<?php
include('includes/footer.php'); 
?>