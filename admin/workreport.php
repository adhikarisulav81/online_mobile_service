<?php
define('TITLE', 'Work Report');
define('PAGE', 'workreport');
include('includes/header.php');
include('../dbConnection.php'); 
session_start();
 if(isset($_SESSION['is_adminlogin'])){
  $aEmail = $_SESSION['aEmail'];
 } else {
  echo "<script> location.href='adminLogin.php'; </script>";
 }
?>
<img class="wave" src="../images/wave.png">

<div class="col-sm-9 col-md-10 mt-5 text-center">
<h3 class="title text-center font-weight-bold mb-5" style="font-family: Arial, Helvetica, sans-serif;"><i class="fas fa-clipboard"></i> WORK <span>REPORT</span></h3>

  <form action="" method="POST" class="d-print-none">
    <div class="form-row">
      <div class="form-group col-md-2">
        <input type="date" class="form-control" id="startdate" name="startdate">
      </div> <span> to </span>
      <div class="form-group col-md-2">
        <input type="date" class="form-control" id="enddate" name="enddate">
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-info" name="searchsubmit" value="Search">
      </div>
    </div>
  </form>
  <?php
 if(isset($_REQUEST['searchsubmit'])){
    $startdate = $_REQUEST['startdate'];
    $enddate = $_REQUEST['enddate'];
    $sql = "SELECT * FROM assignwork_tb WHERE assign_date BETWEEN '$startdate' AND '$enddate'";
    $result = $conn->query($sql);
    if($result->num_rows > 0){
     echo '
     <div class="table-responsive-sm">
     <h3 class="title text-center font-weight-bold text-dark mb-5 mt-5" style="font-family: Arial, Helvetica, sans-serif;">
     <i class="fas fa-clipboard"></i> REPORT <span>DETAILS</span></h3>
  <table class="table">
  <thead>
    <tr>
      <th scope="col">Req ID</th>
      <th scope="col">Request Info</th>
      <th scope="col">Name</th>
      <th scope="col">Address</th>
      <th scope="col">City</th>
      <th scope="col">Mobile</th>
      <th scope="col">Technician</th>
      <th scope="col">Assigned Date</th>
    </tr>
  </thead>
  <tbody>';
  while($row = $result->fetch_assoc()){
    echo '<tr>
    <th scope="row">'.$row["request_id"].'</th>
    <td>'.$row["request_info"].'</td>
    <td>'.$row["requester_name"].'</td>
    <td>'.$row["requester_add2"].'</td>
    <td>'.$row["requester_city"].'</td>
    <td>'.$row["requester_mobile"].'</td>
    <td>'.$row["assign_tech"].'</td>
    <td>'.$row["assign_date"].'</td>
      </tr>';
    }
    echo '<tr>
      <td>
        <form class="d-print-none">
          <button type="submit" class="btn btn-info mt-5" name="Print" onClick="window.print()""><i class="fas fa-print"></i> Print</button>
        </form>
      </td>
      <td>
        <form class="d-print-none" action="workreport.php">
          <button type="submit" class="btn btn-secondary mr-5 mt-5" name="Back""><i class="fas fa-backward"></i> Back</button>
        </form>
      </td>
    </tr>
  </tbody>
  </table>';
  } else {
    echo "<div class='alert alert-warning col-sm-6 mt-2' role='alert'> No Records Found ! </div>";
  }
 }
  ?>
</div>
</div>
</div>

<?php
include('includes/footer.php'); 
?>