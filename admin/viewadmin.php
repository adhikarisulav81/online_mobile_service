<?php
define('TITLE', 'Admin');
define('PAGE', 'viewadmin');
include('includes/header.php'); 
include('../dbConnection.php');
session_start();

 if(isset($_SESSION['is_adminlogin'])){
  $aEmail = $_SESSION['aEmail'];
 } else {
  echo "<script> location.href='adminLogin.php'; </script>";
 }
?>

<img class="wave" src="../images/wave.png">

<div class="col-sm-9 col-md-10">
  <!--Table-->
  <h3 class="title text-center font-weight-bold text-dark mt-5 mb-5" style="font-family: Arial, Helvetica, sans-serif;"><i class="fas fa-users"></i> LIST OF <span>ADMIN</span></h3>
  <?php
    $sql = "SELECT * FROM adminlogin_tb";
    $result = $conn->query($sql);
    if($result->num_rows > 0){
      echo '<div class="table-responsive-sm">';
      echo '<table id="dataTableID" class="table">
        <thead>
        <tr>
          <th scope="col">Admin ID</th>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>';
          while($row = $result->fetch_assoc()){
            echo '<tr>';
              echo '<th scope="row">'.$row["id"]. '.'.'</th>';
              echo '<td>'. $row["name"].'</td>';
              echo '<td>'.$row["email"].'</td>';
              echo '<td>
                <form action="editadmin.php" method="POST" class="d-inline"> 
                  <input type="hidden" name="id" value='. $row["id"] .'>
                  <button type="submit" class="btn mr-2 mt-2" name="view" value="View"><i class="fas fa-eye"></i></button>
                </form>
                <form action="" method="POST" class="d-inline">
                  <input type="hidden" name="id" value='. $row["id"] .'>
                  <button type="submit" class="btn btn-danger mt-2 " name="delete" value="Delete"><i class="far fa-trash-alt"></i></button>
                </form>
              </td>
            </tr>';
          }
        echo '</tbody>
      </table>';
      echo '</div>';
    } else {
      echo "No Records Found.";
    }
if(isset($_REQUEST['delete'])){
  $sql = "DELETE FROM adminlogin_tb WHERE id = {$_REQUEST['id']}";
  if($conn->query($sql) === TRUE){
    // echo "Record Deleted Successfully";
    // below code will refresh the page after deleting the record
    echo '<meta http-equiv="refresh" content= "0;URL=?deleted" />';
    } else {
      echo "Unable to Delete Data";
    }
  }
?>

  </div>
</div>

<div class="text-center">
  <a href="insertadmin.php" class="btn fixed-bottom" title="Add User" style="background-color: #28c38e;"><i class="fas fa-plus fa"></i> Add New Admin</a>
</div>
</div>

<?php
include('includes/footer.php'); 
?>