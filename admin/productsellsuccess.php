<?php
define('TITLE', 'Product Sell Success');
define('PAGE', 'productsellsuccess');
include('includes/header.php'); 
include('../dbConnection.php');
session_start();
 if(isset($_SESSION['is_adminlogin'])){
  $aEmail = $_SESSION['aEmail'];
 } else {
  echo "<script> location.href='adminLogin.php'; </script>";
 }

$sql = "SELECT * FROM customer_tb WHERE custid = {$_SESSION['myid']}";
$result = $conn->query($sql);
if($result->num_rows == 1){
 $row = $result->fetch_assoc();
echo "<img class='wave d-print-none' src='../images/wave.png'>
<div class='table-responsive-sm ml-5'>
<h3 class='title text-center font-weight-bold text-dark mb-5 mt-5' style='font-family: Arial, Helvetica, sans-serif;'>
<i class='fas fa-file-invoice'></i> CUSTOMER <span>BILL</span></h3>
 <table class='table'>
  <tbody>
  <tr>
    <th>Customer ID</th>
    <td>".$row['custid']."</td>
  </tr>
   <tr>
     <th>Customer Name</th>
     <td>".$row['custname']."</td>
   </tr>
   <tr>
     <th>Address</th>
     <td>".$row['custadd']."</td>
   </tr>
   <tr>
   <th>Product</th>
   <td>".$row['cpname']."</td>
  </tr>
   <tr>
    <th>Quantity</th>
    <td>".$row['cpquantity']."</td>
   </tr>
   <tr>
    <th>Price Each</th>
    <td>".$row['cpeach']."</td>
   </tr>
   <tr>
    <th>Total Cost</th>
    <td>".$row['cptotal']."</td>
   </tr>
   <tr>
   <th>Date</th>
   <td>".$row['cpdate']."</td>
  </tr>
   <tr>
    <td>
      <form class='d-print-none'>
        <button type='submit' class='btn btn-info mt-5' name='Print' onClick='window.print()''><i class='fas fa-print'></i> Print</button>
      </form>
    </td>
    <td>
      <form class='d-print-none' action='assets.php'>
        <button type='submit' class='btn btn-secondary mt-5' name='Back''><i class='fas fa-backward'></i> Back</button>
      </form>
    </td>
  </tr>
  </tbody>
 </table> </div>
 ";

} else {
  echo "Failed";
}
?>

<?php
include('includes/footer.php'); 
$conn->close();
?>