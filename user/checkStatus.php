<?php
define('TITLE', 'Check Status');
define('PAGE', 'checkStatus');
include('includes/header.php'); 
include('../dbConnection.php');
session_start();

if($_SESSION['is_login']){
 $rEmail = $_SESSION['rEmail'];
} else {
 echo "<script> location.href='userLogin.php'; </script>";
}
?>
  <img class="wave d-print-none" src="../images/wave.png">
  
<div class="col-sm-6">
  <form action="" class="d-print-none">
    <h3 class="title text-center font-weight-bold text-dark mb-5 mt-5" style="font-family: Arial, Helvetica, sans-serif;">
    <i class="fas fa-truck-moving"></i> CHECK <span>REQUEST STATUS</span></h3>
    <div class="form-group mr-3">
      <i class="fas fa-portrait"></i>
      <label for="checkid">Enter Request ID: </label>
      <input type="text" class="form-control" id="checkid" name="checkid" onkeypress="isInputNumber(event)">
    </div>
    <button type="submit" name="search" class="btn btn-info"><i class="fas fa-search"></i> Search</button>
  </form>
  <?php if(isset($msg)) {echo $msg; } ?>
  
  <?php
  if(isset($_REQUEST['checkid'])){
    if(($_REQUEST['checkid'] == "")){
      // msg displayed if required field missing
      $msg = '<div class="alert alert-warning col-sm-6 mt-2" role="alert"> Fill All Fileds </div>';
    } else {
      $sql = "SELECT * FROM assignwork_tb WHERE request_id = {$_REQUEST['checkid']}";
      $result = $conn->query($sql);
      $row = $result->fetch_assoc();
    if(($row['request_id']) == $_REQUEST['checkid']){ 
      echo '<div class="alert alert-dark mt-4" role="alert">
      Your Request is Already Assigned. </div>';
  ?>

  <h3 class="title text-center mt-5 font-weight-bold mb-5 jumbotron-fluid">ASSIGNED <span>WORK DETAILS</span></h3>
  <div class="table-responsive-sm">
    <table class="table table-bordered">
      <tbody>
        <tr>
          <td>Request ID</td>
          <td>
            <?php if(isset($row['request_id'])) {echo $row['request_id']; } ?>
          </td>
        </tr>
        <tr>
          <td>Request Info</td>
          <td>
            <?php if(isset($row['request_info'])) {echo $row['request_info']; } ?>
          </td>
        </tr>
        <tr>
          <td>Request Description</td>
          <td>
            <?php if(isset($row['request_desc'])) {echo $row['request_desc']; } ?>
          </td>
        </tr>
        <tr>
          <td>Name</td>
          <td>
            <?php if(isset($row['requester_name'])) {echo $row['requester_name']; } ?>
          </td>
        </tr>
        <tr>
          <td>Address Line 1</td>
          <td>
            <?php if(isset($row['requester_add1'])) {echo $row['requester_add1']; } ?>
          </td>
        </tr>
        <tr>
          <td>Address Line 2</td>
          <td>
            <?php if(isset($row['requester_add2'])) {echo $row['requester_add2']; } ?>
          </td>
        </tr>
        <tr>
          <td>City</td>
          <td>
            <?php if(isset($row['requester_city'])) {echo $row['requester_city']; } ?>
          </td>
        </tr>
        <tr>
          <td>State</td>
          <td>
            <?php if(isset($row['requester_state'])) {echo $row['requester_state']; } ?>
          </td>
        </tr>
        <tr>
          <td>Pin Code</td>
          <td>
            <?php if(isset($row['requester_zip'])) {echo $row['requester_zip']; } ?>
          </td>
        </tr>
        <tr>
          <td>Email</td>
          <td>
            <?php if(isset($row['requester_email'])) {echo $row['requester_email']; } ?>
          </td>
        </tr>
        <tr>
          <td>Mobile</td>
          <td>
            <?php if(isset($row['requester_mobile'])) {echo $row['requester_mobile']; } ?>
          </td>
        </tr>
        <tr>
          <td>Assigned Date</td>
          <td>
            <?php if(isset($row['assign_date'])) {echo $row['assign_date']; } ?>
          </td>
        </tr>
        <tr>
          <td>Technician Name</td>
          <td><?php if(isset($row['assign_tech'])) {echo $row['assign_tech']; } ?></td>
        </tr>
        <tr>
          <td>Device Status</td>
          <td>
            <?php if(isset($row['status'])) {echo $row['status']; }?>
          </td>
        </tr>
        <tr>
          <td>Delivery Date</td>
          <td>
            <?php if(isset($row['deliveryDate'])) {echo $row['deliveryDate']; }?>
          </td>
        </tr>
        <tr>
          <td>Customer Sign</td>
          <td></td>
        </tr>
        <tr>
          <td>Technician Sign</td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="text-center">
    <form action="" class="d-print-none d-inline mr-3">
      <button type="submit" class="btn btn-info" name="Print" onClick="window.print()"><i class="fas fa-print"></i> Print</button>
    </form>
    <form class="d-print-none d-inline" action="#">
      <button type="submit" class="btn btn-secondary" name="close"><i class="far fa-times-circle"></i> Close</button>
    </form>
  </div>

  <?php } else {
      echo '<div class="alert alert-dark mt-4" role="alert">
      Your Request might not be assigned. </div>';
    }
  }
}
 ?>
 
</div>

<!-- Only Number for input fields -->
<script>
  function isInputNumber(evt) {
    var ch = String.fromCharCode(evt.which);
    if (!(/[0-9]/.test(ch))) {
      evt.preventDefault();
    }
  }
</script>

<?php
include('includes/footer.php'); 
?>