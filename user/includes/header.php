<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta http-equiv="X-UA-Compatible" content="ie=edge">
 <title>
  <?php echo TITLE ?>
 </title>

 <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="../css/bootstrap.min.css">

 <!-- Font Awesome CSS -->
 <link rel="stylesheet" href="../css/all.min.css">

 <!-- Custome CSS -->
 <link rel="stylesheet" href="../css/style.css">
</head>

<body>
 <!-- Top Navbar -->
<nav class="navbar navbar-dark fixed-top flex-md-nowrap p-0 shadow" style="background-color: #24ad7f;">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="userProfile.php" title="Online Mobile Repair Service Center Management System"><i class="fas fa-mobile"></i> Online Repair</a>
 </nav>

 <!-- Side Bar -->
 <div class="container-fluid mb-5 " style="margin-top:40px;">
  <div class="row">
   <nav class="col-sm-2 sidebar py-5 d-print-none">
   <div class="sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item">
      <a class="nav-link <?php if(PAGE == 'userProfile') { echo 'active'; } ?>" href="userProfile.php">
        <i class="fas fa-user-md"></i>
        User Profile <span class="sr-only">(current)</span>
      </a>
      </li>
      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'submitRequest') { echo 'active'; } ?>" href="submitRequest.php">
       <i class="far fa-share-square"></i>
        Submit Requests
       </a>
      </li>
      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'checkStatus') { echo 'active'; } ?>" href="checkStatus.php">
       <i class="fas fa-truck-moving"></i>
        Track Request Status
       </a>
      </li>
      <li class="nav-item">
       <a class="nav-link <?php if(PAGE == 'userChangepass') { echo 'active'; } ?>" href="userChangepass.php">
       <i class="fas fa-lock"></i>
        Alter Password
       </a>
      </li>
      <li class="nav-item">
       <a class="nav-link" href="../logout.php">
        <i class="fas fa-sign-out-alt"></i>
        Logout
       </a>
      </li>
     </ul>
    </div>
   </nav>