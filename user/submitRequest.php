<?php
define('TITLE', 'Submit Request');
define('PAGE', 'submitRequest');
include('includes/header.php'); 
include('../dbConnection.php');
session_start();

if($_SESSION['is_login']){
 $rEmail = $_SESSION['rEmail'];
} else {
 echo "<script> location.href='userLogin.php'; </script>";
}

if(isset($_REQUEST['submitrequest'])){

 // Checking for Empty Fields
 if(($_REQUEST['requestinfo'] == "") || ($_REQUEST['requestdesc'] == "") || ($_REQUEST['requestername'] == "") || ($_REQUEST['requesteradd1'] == "") || 
 ($_REQUEST['requesteradd2'] == "") || ($_REQUEST['requestercity'] == "") || ($_REQUEST['requesterstate'] == "") || ($_REQUEST['requesterzip'] == "") || 
 ($_REQUEST['requesteremail'] == "") || ($_REQUEST['requestermobile'] == "") || ($_REQUEST['requestdate'] == "")){
  // msg displayed if required field missing
  $msg = '<div class="alert alert-warning col-sm-6 ml-5 mt-2" role="alert"> Fill All Fileds </div>';
 } else {
   // Assigning User Values to Variable

   $file = addslashes(file_get_contents($_FILES["image"]["tmp_name"]));

   $rinfo = $_REQUEST['requestinfo'];
   $rdesc = $_REQUEST['requestdesc'];
   $rname = $_REQUEST['requestername'];
   $radd1 = $_REQUEST['requesteradd1'];
   $radd2 = $_REQUEST['requesteradd2'];
   $rcity = $_REQUEST['requestercity'];
   $rstate = $_REQUEST['requesterstate'];
   $rzip = $_REQUEST['requesterzip'];
   $remail = $_REQUEST['requesteremail'];
   $rmobile = $_REQUEST['requestermobile'];
   $rdate = $_REQUEST['requestdate'];

   $sql = "INSERT INTO submitrequest_tb(image, request_info, request_desc, requester_name, requester_add1, requester_add2, 
   requester_city, requester_state, requester_zip, requester_email, requester_mobile, request_date) 
   VALUES ('$file', '$rinfo', '$rdesc', '$rname', '$radd1', '$radd2', '$rcity', '$rstate', '$rzip', '$remail', '$rmobile', '$rdate')";
   if($conn->query($sql) == TRUE){
    // below msg display on form submit success
    $genid = mysqli_insert_id($conn);
    $msg = '<div class="alert alert-success col-sm-6 ml-5 mt-2" role="alert"> Request Submitted Successfully Your' . $genid .' </div>';
    session_start();
    $_SESSION['myid'] = $genid;
    echo "<script> location.href='submitrequestsuccess.php'; </script>";
    // include('submitrequestsuccess.php');
   } else {
    // below msg display on form submit failed
    $msg = '<div class="alert alert-danger col-sm-6 ml-5 mt-2" role="alert"> Unable to Submit Your Request </div>';
   }
 }
}
?>

<img class="wave" src="../images/wave.png">
<div class="col-sm-9 col-md-10">
  <form class="" action="" method="POST" enctype="multipart/form-data">
    <h3 class="title text-center font-weight-bold text-dark mb-5 mt-5" style="font-family: Arial, Helvetica, sans-serif;">
    <i class="far fa-share-square"></i> SUBMIT <span>REQUEST FORM<span></h3>


    <div class="form-group">
      <label for="inputRequestInfo"><i class="fas fa-info-circle"></i> Request Info</label>
      <input type="text" class="form-control" id="inputRequestInfo" placeholder="Request Info" name="requestinfo">
    </div>
    <div class="form-group">
      <label for="inputRequestDescription"><i class="fas fa-pen-nib"></i> Description</label>
      <input type="text" class="form-control" id="inputRequestDescription" placeholder="Write Description" name="requestdesc">
    </div>
    <div class="form-group">
      <label for="inputName"><i class="fas fa-users"></i> Name</label>
      <input type="text" class="form-control" id="inputName" placeholder="Enter Your Name" name="requestername">
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputAddress"><i class="fas fa-map-marker-alt"></i> Temporary Address</label>
        <input type="text" class="form-control" id="inputAddress" placeholder="Enter Temporary Address" name="requesteradd1">
      </div>
      <div class="form-group col-md-6">
        <label for="inputAddress2"><i class="fas fa-map-marker-alt"></i> Permanent Address</label>
        <input type="text" class="form-control" id="inputAddress2" placeholder="Enter Permanent Address" name="requesteradd2">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputCity"><i class="fas fa-city"></i> City</label>
        <input type="text" class="form-control" id="inputCity" placeholder="Enter City" name="requestercity">
      </div>
      <div class="form-group col-md-4">
        <label for="inputState"><i class="fas fa-flag"></i> State</label>
        <input type="text" class="form-control" id="inputState" placeholder="Enter State" name="requesterstate">
      </div>
      <div class="form-group col-md-2">
        <label for="inputZip"><i class="fas fa-globe-asia"></i> Zip Code</label>
        <input type="text" class="form-control" id="inputZip" placeholder="Enter Zip Code" name="requesterzip" onkeypress="isInputNumber(event)">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail"><i class="far fa-envelope"></i> Email</label>
        <input type="email" class="form-control" id="inputEmail" placeholder="Enter Email Address" name="requesteremail">
      </div>
      <div class="form-group col-md-2">
        <label for="inputMobile"><i class="fas fa-mobile"></i> Mobile</label>
        <input type="text" class="form-control" id="inputMobile" placeholder="Enter Mobile Number" name="requestermobile" onkeypress="isInputNumber(event)">
      </div>
      <div class="form-group col-md-2">
        <label for="inputDate"><i class="fas fa-calendar-alt"></i> Requested Date</label>
        <input type="date" class="form-control" id="inputDate" name="requestdate">
      </div>
      

    <div class="form-group">
      <label for="image"><i class="fas fa-info-circle"></i> Upload ID Image</label>
      <input type="file" class="form-control" id="image" name="image">
    </div>

    </div>
    <button type="submit" class="btn btn-success mt-2" name="submitrequest"><i class="fas fa-paper-plane"></i> Submit</button>
    <button type="reset" class="btn btn-secondary mt-2"><i class="fas fa-cut"></i> Reset</button>
  </form>
  <!-- below msg display if required fill missing or form submitted success or failed -->
  <?php if(isset($msg)) {echo $msg; } ?>
  </div>
</div>
</div>

<!-- Only Number for input fields -->
<script>
  function isInputNumber(evt) {
    var ch = String.fromCharCode(evt.which);
    if (!(/[0-9]/.test(ch))) {
      evt.preventDefault();
    }
  }
</script>

<?php
include('includes/footer.php'); 
$conn->close();
?>