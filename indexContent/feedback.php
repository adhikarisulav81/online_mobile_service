<!-- Start Feedback Section -->
<div id="feedback" data-aos="fade-left">
  <section id="feedback">
  <div class="container py-5 text-center">
    <div class="row">
      <div class="col title">
        <h2 class="text-center mt-5">USER<span> FEEDBACK</span></h2>
      </div>
    </div>

    <div class="row text-dark">
      <div class="col-lg-3 col-md-6">
        <div class="card">
          <div class="card-body">
            <img src="images/bik.png" alt="" class="img-fluid rounded-circle w-50 mb-3">
            <h3>Bikram Shrestha</h3>
            <h5>Businessman, Client</h5>
            <p>The outcome & review of this application is phenomenal.</p>
            <div class="d-flex flex-row justify-content-center">
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-facebook"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-instagram"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="card">
          <div class="card-body">
            <img src="images/aav.png" alt="" class="img-fluid rounded-circle w-50 mb-3">
            <h3>Avash Neupane</h3>
            <h5>User</h5>
            <p>Features included in this application are praiseworthy.</p>
            <div class="d-flex flex-row justify-content-center">
            <div class="p-4">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-facebook"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-instagram"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="col-lg-3 col-md-6">
        <div class="card">
          <div class="card-body">
            <img src="images/mahenk.png" alt="" class="img-fluid rounded-circle w-50 mb-3">
            <h3>Mahendra Batha</h3>
            <h5>Consumer</h5>
            <p>Great application to get your device issue serviced.</p>
            <div class="d-flex flex-row justify-content-center">
            <div class="p-4">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-facebook"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-instagram"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="card">
          <div class="card-body">
            <img src="images/kau.jpg" alt="" class="img-fluid rounded-circle w-50 mb-3">
            <h3>Kaushal Adhikari</h3>
            <h5>User</h5>
            <p>The application was easy to utilize and user friendly.</p>
            <div class="d-flex flex-row justify-content-center">
            <div class="p-4">
                <a href="#">
                  <i class="fab fa-twitter"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-facebook"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-instagram"></i>
                </a>
              </div>
              <div class="p-4">
                <a href="#">
                  <i class="fab fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
  </section>
</div>
<!-- End Feedback Section -->