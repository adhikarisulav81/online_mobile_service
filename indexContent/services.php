<!-- Start Services -->
<div class="text-center mt-5" id="services" data-aos="fade-down">
<h2 class="text-center">SERVICES</h2>

<div class="wrapper1 mt-5">
  <div class="box">
    <div class="front-face">
      <div class="icon">
      <i class="fas fa-id-card-alt fa-4x" style="color: #E27D60;"></i>
      </div>
      <span>Track Request Service Status</span>
    </div>
    <div class="back-face">

      <a href="indexContent/checkStatusFromIndex.php"><i class="fas fa-id-card-alt fa-8x" style="color: #E27D60;"></i></a>
    </div>
  </div>

  <div class="box">
    <div class="front-face">
      <div class="icon">
      <i class="fas fa-cogs fa-4x" style="color: maroon;"></i>
      </div>
      <span>Fault Repair</span>
    </div>
    <div class="back-face">
    <a href="#"><i class="fas fa-cogs fa-8x" style="color: maroon;"></i></a>
  </div>
</div>

<div class="box">
    <div class="front-face">
      <div class="icon">
      <i class="fas fa-wrench fa-4x" style="color: #28c38e;"></i>
      </div>
      <span>Preventive Maintenance</span>
    </div>
    <div class="back-face">

    <a href="#"><i class="fas fa-wrench fa-8x" style="color: #28c38e;"></i></a>
    </div>
  </div>
  
<div class="box">
    <div class="front-face">
      <div class="icon">
      <i class="fas fa-tv fa-4x" style="color: grey;"></i>
      </div>
      <span>Submit Request</span>
    </div>
    <div class="back-face">

    <a href="#"><i class="fas fa-tv fa-8x" style="color: grey;"></i></a>
    </div>
  </div>

</div>

<!-- End Services -->