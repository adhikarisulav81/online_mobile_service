<!-- Start Blog  -->
<div id="blog" data-aos="fade-right">
  <!-- Start Blog Jumbotron -->
  <div class="container mb-5">
    <!-- Start Blog Container -->
    <h2 class="title text-center">BLOGS</h2>
    <div class="row mt-5">
      <div class="col-lg-3 col-sm-6">
        <!-- Start Blog 1st Column-->
        <div class="card shadow-lg mb-2">
          <div class="card-body text-center">
            <img src="images/chargeblog.jpg" class="img-fluid mb-3" style="border-radius: 50px;">
            <h5 class="card-title text-dark font-weight-bold mb-4">My Phone Won't Charge ?</h5>

            <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target=".bd-example-modal-lg" style="background-color: #28c38e;">
              Read More..
            </button>

            <!-- Modal -->          
            <div class="modal fade bd-example-modal-lg" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-dark" style="background-color: #24ad7f;">
                  <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">My Phone Won't Charge ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>

                  <div class="modal-body">
                    <p class="text-justify">
                        When you get home from a long day of work or a busy day,
                        settling in and charging your phone back up is sort of a daily ritual. If
                        you’ve ever arrived home, plopped down in your seat and realized that your
                        phone wasn’t charging, this is your guide. Let’s look at some signs and reasons
                        the charging port is dying.
                    </p>

                    <img src="images/chargeblog.jpg" alt="" height="100%" width="100%">

                    <h3 class="font-weight-bold mt-4">Is it your cord or wall adapter?</h3>
                    <p class="text-justify">
                        It is a good idea to look at these two before looking at
                        anything with the phone. First, try plugging only the USB cord into a computer,
                        laptop, or any USB port. If your phone charges, then it is most likely the
                        adapter that plugs into the wall. They are at most cell phone stores or gas
                        stations if you need one.

                        If it is does not charge, then try another USB cord and see if that works.
                        Lastly, try another outlet to see if there are issues with it. If after these
                        steps your phone does not want to charge, it may be time to look at the device
                        itself.
                    </p>

                    <h3 class="font-weight-bold mt-4">Could it be your charging port? Should I fix it myself?</h3>
                    <p class="text-justify">
                    If your phone has wireless charging and you have a pad for it, I would suggest trying it first. If it works, you at least know that it is just the charging port and not a deeper issue. Once we have tried all these steps, it may be time to call it and repair your charging port after all.

                    On most models, the charging port comes into contact with
                    different network cables, headphone jack, and even the vibration motor for your
                    phone. Unless you have experience, it is largely unadvised to switch these out
                    without guidance due to the possibility of damaging internal parts.
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

            </div>
          </div>
        </div> <!-- End Blog 1st Column-->

        <div class="col-lg-3 col-sm-6">
          <!-- Start Blog 2nd Column-->
          <div class="card shadow-lg mb-2">
            <div class="card-body text-center">
              <img src="images/wifiblog.jpg" class="img-fluid mb-3" style="border-radius: 50px;">
              <h5 class="card-title text-dark font-weight-bold mb-4">Phone can’t connect to Wi-Fi ?</h5>
              <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target=".bd-example1-modal-lg" style="background-color: #28c38e;">
              Read More..
            </button>

            <!-- Modal -->
            <div class="modal fade bd-example1-modal-lg" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-dark" style="background-color: #24ad7f;">
                  <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Phone can’t connect to Wi-Fi ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p class="text-justify mb-4">
                        Can’t connect to your email, internet, or anything else on
                        your phone? Follow these general steps and you’ll be connected once again. This
                        will help you determine whether or not it is a software, configuration, or hardware
                        problem.
                    </p>

                    <img src="images/wifiblog.jpg" alt="" height="100%" width="100%">

                    <h3 class="font-weight-bold mt-4">Wi-Fi button First.</h3>
                    <p class="text-justify mb-4">
                        The very first thing you want to check is if your Wi-Fi is actually enabled.
                        Sometimes we may forget to turn it back on or it was not on in the first place.
                        Regardless, what you want to do is open up your phone and go to the main home
                        menu. Whether you are on iPhone or Android, swiping down from the top of the
                        screen should bring up a menu of icons that turn certain functions like Bluetooth
                        on or off. Simply look for the Wi-Fi icon then continue.

                        If this is the issue, you will see it light up and prompt
                        you to join a Wi-Fi network (unless it automatically connects to a previous
                        network). Voila! If it connects, great. If it does not, here’s some more
                        symptoms.
                    </p>

                    <h3 class="font-weight-bold mt-4">Accidental airplane Mode.</h3>
                    <p class="text-justify mb-4">
                        Whether or not you took a flight recently, I would check
                        this option just to be sure. Sometimes it accidentally gets activated and
                        blocks your phone from connecting to Wi-Fi. It is in the same place as the Wi-Fi
                        button, and is always a picture of an airplane. Make sure to turn this option
                        OFF and check again to see if you can connect.
                    </p>

                    <h3 class="font-weight-bold mt-4">Your phone might need an update.</h3>
                    <p class="text-justify mb-4">
                        If your phone isn’t set to automatically update, it would be
                        good to make sure your phone’s software is up to date. Manufacturers will
                        release updates that change how your phone interacts with networks. If you have
                        mobile data, go to settings and see if you need a quick software upgrade.
                    </p>

                    <h3 class="font-weight-bold mt-4">Before we mess with settings....</h3>
                    <p class="text-justify mb-4">
                        Before we completely reset the network settings, I would
                        recommend resetting your phone. Simply power your device down and power it back
                        up. In simple terms, it will “refresh” the setup on the phone, and it will possibly
                        fix your issue.
                    </p>

                    <h3 class="font-weight-bold mt-4">Reset your network settings.</h3>
                    <p class="text-justify mb-4">
                        The last step I would try if nothing else works is to reset
                        all network settings. This is usually found under Settings->General->Network settings

                        There will be an option to either forget your current
                        network (Don’t worry, it can still find it again, it will just forget your
                        current settings), or you can reset all network settings if that does not fix
                        the issue.

                        Hopefully after these steps, you will be connected to Wi-Fi once again. If you are not able to connect after all, it may turn out to be a
                        hardware problem. If you’re charging port has become damaged or has failed, it may cause Wi-Fi connectivity issues.

                        In the worst case scenario, if you have not had luck with
                        any of these steps, Quick Mobile Repair offers free diagnosis on any device, so
                        stop in to one of our locations and a technician can let you know exactly what is happening.
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

            </div>
          </div>
        </div> <!-- End Blog 2nd Column-->

        <div class="col-lg-3 col-sm-6">
          <!-- Start Blog 3rd Column-->
          <div class="card shadow-lg mb-2">
            <div class="card-body text-center">
              <img src="images/glassbrokeblog.jpg" class="img-fluid mb-3" style="border-radius: 50px; height: 140px;">
              <h5 class="card-title text-dark font-weight-bold mb-4">Broke Phone? Don’t Replace, Repair!</h5>

              <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target=".bd-example2-modal-lg" style="background-color: #28c38e;">
              Read More..
            </button>

            <!-- Modal -->
            <div class="modal fade bd-example2-modal-lg" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-dark" style="background-color: #24ad7f;">
                  <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Broke Phone? Don’t Replace, Repair!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p class="text-justify mb-4">
                        We’ve all dropped our phone. As it tumbles to the ground, you have just enough time to hope the impact isn’t too bad. 
                        Sometimes, luck is on your side, and your phone is intact. Other times … not so much. 
                        When your screen is cracked or the phone is otherwise damaged, you have a few options. 
                        Depending on your cell phone carrier and whether or not you are under a contract, you may be able to upgrade to a new phone for a relatively low price. 
                        However, the environmental price for a new phone is high–it won’t hurt your wallet, but you should still care about it.
                    </p>

                    <img src="images/glassbrokeblog.jpg" alt="" height="100%" width="100%">

                    <h3 class="font-weight-bold mt-4">E-Waste – A New Kind of Garbage.</h3>
                    <p class="text-justify mb-4">
                        Cell phones are considered electronic waste, or e-waste. When you get a new phone and your old one becomes junk, chances are it is headed for an electronic landfill. 
                        Electronics contain all sorts of chemicals and toxins that can leach into soil and contaminate ground water if not disposed of in a responsible way. 
                        Even when recycled properly, a significant percentage of the old phone still ends up in a landfill.

                        The Scientific American reports that cell phone sales grew by 23% in 2014, but the EPA estimates that only 27% of e-waste is recycled. 
                        At this rate, our consumption will quickly outpace our ability to recycle the discarded products. 
                        Reports show that “If Americans recycled the approximately 130 million cell phones that are disposed of annually, enough energy would be saved to power more than 24,000 homes in a year.”
                    </p>

                    <h3 class="font-weight-bold mt-4">Reduce, Reuse, Recycle – You Can Make a Difference.</h3>
                    <p class="text-justify mb-4">
                        There are several ways that you can help reduce e-waste.
                        <ul>
                            <li>
                                By doubling the length of time you keep an electronic, you cut the amount of e-waste that you produce in half! So when you drop your phone, take it to a cell phone repair store and have it repaired!
                            </li>
                            <li>
                                If your phone is beyond repair, consider purchasing a refurbished phone instead of a new one. You’ll likely save money and, if you buy the same model you had, you can save time because you won’t need to learn to use different features.
                            </li>
                            <li>
                                Make sure any phone that cannot be repaired is taken to a recycler that has been certified to properly and safely dispose of old electronics.
                            </li>
                        </ul>

                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

            </div>
          </div>
        </div> <!-- End Blog 3rd Column-->

        <div class="col-lg-3 col-sm-6">
          <!-- Start Blog 4th Column-->
          <div class="card shadow-lg mb-2">
            <div class="card-body text-center">
              <img src="images/unlockblog.jpg" class="img-fluid mb-3" style="border-radius: 50px; height: 140px;">
              <h5 class="card-title text-dark font-weight-bold mb-4">Phone Unlock Problem ?</h5>
              <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target=".bd-example3-modal-lg" style="background-color: #28c38e;">
              Read More..
            </button>

            <!-- Modal -->
            <div class="modal fade bd-example3-modal-lg" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-dark" style="background-color: #24ad7f;">
                  <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Phone Unlock Problem ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p class="text-justify mb-4">
                        On any model of the iPhone, there is a security feature that will ensure that anybody that tries to guess your passcode can’t get in. After so many tries, your phone will be disabled for a short time (Typically a minute) and then you can try your passcode again. After too many attempts, it will be disabled for longer and longer.

                        Anybody that has kids has most had this happen before, a lot.  It’s not just people that can lock your device either, If your screen is damaged, your digitizer (the part in your screen that recognizes your fingers) can malfunction, and accidentally call emergency services/Disable your
                        screen. If your phone gets permanently disabled, just follow these steps and
                        your phone will be functional again.
                    </p>

                    <img src="images/unlockblog.jpg" alt="" height="100%" width="100%">

                    <h3 class="font-weight-bold mt-4">When the phone is disabled?</h3>
                    <p class="text-justify mb-4">
                        When your phone is disabled, the screen will read “iPhone is
                        disabled, please connect to iTunes”. First, you’ll want to download iTunes on
                        whichever computer you want to use. When you
                        have downloaded it, open it and connect your phone. Grab the charging cord that
                        came with your phone and connect it by USB to your computer. If it does not
                        have the “connect to iTunes” message you can sync your phone (given you
                        remember the passcode). Whether you can sync it or not, the next step is the
                        same. You’ll want to restore your iPhone. Hopefully you made a backup
                        previously, but if you did not, unfortunately you’ll still have to restore your
                        phone, there is no way around the disabled lock.
                    </p>

                    <h3 class="font-weight-bold mt-4">Finally?</h3>
                    <p class="text-justify mb-4">
                        When it restores, it will ask you for your iCloud
                        information. Once you enter this, your phone will be back and running. It will
                        restore your backup or take you to setup, just follow the steps on screen and
                        you’ll have your phone back. If you don’t have access to a computer, or don’t
                        have time to complete these steps, any technician at Quick Mobile Repair can
                        take care of it for you.
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

              
            </div>
          </div>
        </div> <!-- End Blog 4th Column-->


        <div class="col-lg-3 col-sm-6">
          <!-- Start Blog 5th Column-->
          <div class="card shadow-lg mb-2">
            <div class="card-body text-center">
              <img src="images/backglassblog.jpg" class="img-fluid mb-3" style="border-radius: 50px;">
              <h5 class="card-title text-dark font-weight-bold mb-4">Back Glass Replacement ?</h5>

              <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target=".bd-example4-modal-lg" style="background-color: #28c38e;">
              Read More..
            </button>

            <!-- Modal -->
            <div class="modal fade bd-example4-modal-lg" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-dark" style="background-color: #24ad7f;">
                  <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Back Glass Replacement ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p class="text-justify mb-4">
                        Long gone are the days of having a phone with a removable back. 
                        Unless you have an older Samsung or a Motorola, you may be questioning how to remove the rear of the phone to replace the battery, or just to replace the back of it with new glass! 
                        Let’s walk through exactly what is going on underneath the glass.
                    </p>

                    <img src="images/backglassblog.jpg" alt="" height="100%" width="100%">

                    <h3 class="font-weight-bold mt-4">How is at attached?</h3>
                    <p class="text-justify mb-4">
                        The back glass is a flat piece of glass that fits in the frame of the phone. 
                        There is usually a ring of adhesive that goes around the device that keeps it on the back of the phone. 
                        If it is cracked, It will need to be removed carefully with heat and the proper tools, otherwise it may crack more and create a dust cloud of tiny glass (not fun).

                        It is important to note that underneath the back glass is where your fingerprint sensor may be on newer models, it is important to make sure you don’t go too far past the edge, otherwise you may damage the cable there.
                    </p>

                    <h3 class="font-weight-bold mt-4">How do I remove it? What tools do I need?</h3>
                    <p class="text-justify mb-4">
                        The thing that is most important is to have heat. Without heat it will shatter very easily. 
                        If you have a heat gun, try heating around the edges a bit at a time to loosen the adhesive. 
                        Now that it is heated up, we can start with the tedious part. 
                        You want to either use a suction cup to create a small opening or use a thin plastic card to create the opening in between the back of the phone and the glass. 
                        Once you have the opening, you can keep it open by inserting a thin plastic card or a guitar pick sized object. 
                        After you have your opening, keep heating and working along the edge with your pry tool or plastic card. 
                        It helps to drop a little alcohol on your tool to cut through the adhesive. Once you work all the way around, it should come off fairly easily.
                    </p>

                    <h3 class="font-weight-bold mt-4">How do I put it back on?</h3>
                    <p class="text-justify mb-4">
                        This part is fairly simple, the replacement back glass that you order may have adhesive already applied. 
                        If it doesn’t there is different types of adhesive and pre-made strips that can be used around the edge of the glass. 
                        Once you have this applied, you want to first connect any cables underneath, then lay the back glass back on to the frame. 
                        Be careful that it does not get caught on the camera or it may crack the glass again.
                    </p>

                    <h3 class="font-weight-bold mt-4">Don't want to risk it?</h3>
                    <p class="text-justify mb-4">
                        If all of this sounds like a huge headache and something that might go wrong, Quick Mobile Repair offers this service at all of our locations. 
                        We have the back glass for the majority of Samsung models and it only takes us 20 minutes or less to repair. 
                        On top of a 1-year warranty and low costs, it may be worth it to have us take care of it! 
                        Having broken glass is never fun and we’d like to help you get your Samsung back and stylish as soon as possible!
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

            </div>
          </div>
        </div> <!-- End Blog 5th Column-->


        <div class="col-lg-3 col-sm-6">
          <!-- Start Blog 6th Column-->
          <div class="card shadow-lg mb-2">
            <div class="card-body text-center">
              <img src="images/touchblog.jpg" class="img-fluid mb-3" style="border-radius: 50px; height: 160px;">
              <h5 class="card-title text-dark font-weight-bold mb-4">Mobile Screen Touch Problem !</h5>

              <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target=".bd-example5-modal-lg" style="background-color: #28c38e;">
              Read More..
            </button>

            <!-- Modal -->
            <div class="modal fade bd-example5-modal-lg" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-dark" style="background-color: #24ad7f;">
                  <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Mobile Screen Touch Problem !</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p class="text-justify mb-4">
                        What is the most frustrating thing you can think of off the top of your head? Stubbing your toe? Misplacing your keys? 
                        What about your phone being completely unresponsive to touch? 
                        You don’t have to drop your phone or mess up a system update for this to happen, and your iPhone 6 or 7 might be just be a short time away from it happening to you. 
                        This may seem crazy, but a lot of micro-soldering specialists are pointing to an apple manufacturing flaw as the reason for this issue.
                    </p>

                    <img src="images/touchblog.jpg" alt="" height="100%" width="100%">

                    <h3 class="font-weight-bold mt-4">How To Tell If Your Phone Has Touch Issue?</h3>
                    <p class="text-justify mb-4">
                        There are 2 VERY clear symptoms of Touch IC, one symptom will get progressively worse, and the other is present in most cases of touch IC.  
                        If you feel that your touch is slow, or not responding correctly, you are most likely experiencing this. 
                        When you power your phone, you will see grey bars, lines or squares in the top of your display. 
                        This is absolutely being caused by these issues. The 2nd sign doesn’t show up every time, but if you repeatedly try to  touch your screen, and it not responding, changing the screen will not help.
                    </p>

                    <h3 class="font-weight-bold mt-4">Why is Touch IC happening?</h3>
                    <p class="text-justify mb-4">
                        Touch IC is caused by something below the screen, below the LCD, on the motherboard of the phone. 
                        There is a motherboard chip called Touch IC that translates our fingers into a language the phone can understand. 
                        The design flaw that we talked about earlier? They didn’t secure the chip properly to the board during manufacturing. 
                        It starts lifting and cracking, and our phones stop working.
                    </p>

                    <h3 class="font-weight-bold mt-4">How Do I Fix This?</h3>
                    <p class="text-justify mb-4">
                        Save yourself a trip to the apple store. It will be retired by any apple guru you talk to. 
                        They don’t make these repairs in store, so you’ll be waiting 1-2 hours for them to sell you a new phone essentially. 
                        If you don’t want to buy a new phone, there is a fix to your issues. 
                        If you can find a qualified Technician, they can fix and secure that chip with soldering equipment and get your phone functional again.
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

            </div>
          </div>
        </div> <!-- End Blog 6th Column-->

        <div class="col-lg-3 col-sm-6">
          <!-- Start Blog 7th Column-->
          <div class="card shadow-lg mb-2">
            <div class="card-body text-center">
              <img src="images/commonproblemblog.jpg" class="img-fluid mb-3" style="border-radius: 50px;">
              <h5 class="card-title text-dark font-weight-bold mb-4">Common Mobile Phone Problems !</h5>

              <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target=".bd-example6-modal-lg" style="background-color: #28c38e;">
              Read More..
            </button>

            <!-- Modal -->
            <div class="modal fade bd-example6-modal-lg" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-dark" style="background-color: #24ad7f;">
                  <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Common Mobile Phone Problems !</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p class="text-justify mb-4">
                        The most common problem for all models is a cracked iPhone screen which requires the glass to be replaced. 
                        The phone screen is one of the most fragile parts of an iPhone, leading to it being repaired more often than anything else. 
                        In order to keep up with the high demand for iPhone screen replacements, we keep a large stock of the highest grade LCDs for a wide range of models, from the iPhone 6 all the way up to the most current iPhone 11 Pro Max. 
                        Although broken and cracked glass screens are the most common problem for iPhones, there are things you can do to prevent this from happening. 
                        A high quality tempered glass screen protector has the ability to protect your phone from damage if you accidentally drop or scratch it. 
                        In addition to a tempered glass screen protector, a quality phone case can also increase your device’s protection from accidental damage and a cracked screen. 
                        We carry both tempered glass screen protectors as well as a large range of quality phone cases for all iPhone models at every one of our locations. 
                        To find out more, call a store near you.
                    </p>

                    <img src="images/commonproblemblog.jpg" alt="" height="100%" width="100%">

                    <h3 class="font-weight-bold mt-4">Battery Issues.</h3>
                    <p class="text-justify mb-4">
                        Although all iPhones have the potential to succumb to battery problems, it is uncommon to see these problems in iPhones that are less than two years old. 
                        A majority of our battery replacements are done to increase the total usage time of a device from a full charge, as well as to stop the phone from shutting down prematurely when the battery level has not yet reached 1%. 
                        These two issues typically arise after two to three years of regular usage, depending on how well the iPhones battery is maintained. 
                        Leaving an iPhone plugged in for extended periods of time after it is fully charged and subjecting it to extreme temperatures are some of the ways a device less than two years of age can start having battery issues. 
                        Once an iPhone reaches three years of age, it is very common for it to need a battery replacement, a majority of the devices that come to one of our locations with battery issues are around this age. 
                        Currently, the most common iPhone models we see with battery issues are iPhone 6’s and iPhone 7’s, however in recent months we have started to see more iPhone 8’s and iPhone X’s have these problems.
                    </p>

                    <h3 class="font-weight-bold mt-4">Charging Ports.</h3>
                    <p class="text-justify mb-4">
                        If a person is having an issue with the charging port on their iPhone it can be the result of many different things, 
                        however, the most common issues we see related to iPhone charging ports is that they are clogged with dirt or debris, or a metallic object has gotten stuck in them causing a short circuit which renders the charging port useless. 
                        In either of these scenarios, replacing the charging port is the best way to bring your iPhone back to life and restore that factory fresh click you hear when you plug in the charger which tends to fade over time from plugging and unplugging the cord over and over again. 
                        Currently, we see iPhone 8’s, iPhone 7’s and iPhone 6’s needing charging port repairs more than any other models as it can take a couple years for the build-up of dirt and debris to become noticeable, 
                        however it can largely depend on the level of care you take with your device. 
                        As with most iPhone issues, proper care and prevention can delay these issues from happening for a much longer period of time. 
                        It is recommended that you take your iPhone into any of our locations for a low-cost internal cleaning to prevent your device from having any problems with the charging port.
                    </p>

                    <h3 class="font-weight-bold mt-4">Back Glass Damage.</h3>
                    <p class="text-justify mb-4">
                        Starting with the iPhone 8 series going forward, these devices utilize a back glass panel instead of an aluminum back like some of the earlier iPhone models, 
                        which allow the device to use the wireless charging feature. 
                        Wireless charging requires the back of the phone to be made of this glass so the magnetic field from the wireless charger properly transfers power to the coils in the back of the device. 
                        Just like the front glass screen on these iPhone models, the rear glass is very delicate and can become broken or cracked very easily if proper care is not taken. 
                        Due to the nature of these glass backs being similar to a screen in terms of strength, structure, and potential exposure to physical damage, 
                        we see almost as many broken back glass iPhones as we see front glass screens. 
                        We utilize an intricate machine that uses a special laser to remove this glass without damaging the wireless charging coils inside the device, 
                        as well as the other more delicate aspects of the iPhone such as the camera. 
                        Because of the nature of the back glass, it is no surprise that we see many iPhone 8’s, iPhone X’s and iPhone 11’s needing both a front glass and rear glass repair. 
                        In fact, since the launch of the iPhone 11, we’ve seen almost the same volume of damages to this back glass as we have the front glass on the earlier models like the iPhone 6 and iPhone 7. 
                        Just like the delicate front glass screen, proper preventative measures such as a quality phone case can increase the longevity of your back glass and keep it from becoming cracked or scratched. 
                        You can pick up one of our top of the line iPhone cases and tempered glass screen protectors from any of our stores, as well as book your next iPhone repair.
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

            </div>
          </div>
        </div> <!-- End Blog 7th Column-->

        <div class="col-lg-3 col-sm-6">
          <!-- Start Blog 7th Column-->
          <div class="card shadow-lg mb-2">
            <div class="card-body text-center">
              <img src="images/repairblog.jpg" class="img-fluid mb-3" style="border-radius: 50px; height: 160px;">
              <h5 class="card-title text-dark font-weight-bold mb-4">Why Use a Repair Service?</h5>
              <!-- <p class="card-text text-dark">My Name is Sulav Adhikari.</p> -->
              <!-- <a href="#blog" class="btn"  style="background-color: #28c38e;">Read More..</a> -->

              <!-- Button trigger modal -->
            <button type="button" class="btn" data-toggle="modal" data-target=".bd-example7-modal-lg" style="background-color: #28c38e;">
              Read More..
            </button>

            <!-- Modal -->
            <div class="modal fade bd-example7-modal-lg" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-dark" style="background-color: #24ad7f;">
                  <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="exampleModalScrollableTitle">Why Use a Repair Service?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p class="text-justify mb-4">
                        Have a broken cell phone, but don’t know whether where to take it? Are you out of warranty and worried about the price? 
                        We got you covered for all your worries at our cell phone repair store. Here are three reasons why you should come in with your phone to us.
                    </p>

                    <img src="images/repairblog.jpg" alt="" height="100%" width="100%">

                    <h3 class="font-weight-bold mt-4">Cheaper.</h3>
                    <p class="text-justify mb-4">
                        It’s cheaper. Many phone owners won’t get their screens fixed because of the sticker price for a repair on an item that’s out-of-warranty. 
                        However, we offer competitive prices for repairs that are affordable for owners and cheaper than average price ranges for out-of-warranty repairs. 
                        We also offer a 30-day warranty, no questions asked, for repairs we do on your mobile device.
                    </p>

                    <h3 class="font-weight-bold mt-4">Quick service.</h3>
                    <p class="text-justify mb-4">
                        At many places, you might encounter long waits or multiple meetings over a repair. 
                        It can be frustrating to call in day after day, and not know when you’ll receive your phone back. 
                        If you want quick service to get your phone as good as new, we have repairs on iPhones done in 30 minutes, and other products within a day or two. 
                        There’s no need to ship it out and spend weeks waiting, constantly wondering when your phone or tablet will get back. 
                        You bring it in, and you get it back, simple as that.
                    </p>

                    <h3 class="font-weight-bold mt-4">Quality repairs.</h3>
                    <p class="text-justify mb-4">
                        We’ve come across many who have came to us as a last resort, especially after trying (and failing) to repair their phones on their own. 
                        Sometimes, these customers come with stories of also being scammed by online deals for repair kits that don’t work. 
                        By visiting us, you’re going to get a quality, quick repair by an expert, versus trying to do-it-yourself. 
                        At our repair store, you’re guaranteed that we will have the right materials for your broken iPhone, iPad, Android phone or Android tablet. 
                        There’s no worry about ordering parts that are actually wrong for your phone, or getting scammed out of quality materials.
                    </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

            </div>
          </div>
        </div> <!-- End Blog 7th Column-->

      </div> <!-- End Blog Row-->
    </div> <!-- End Blog Container -->
  </div> 
  <!-- End Blog Jumbotron -->
