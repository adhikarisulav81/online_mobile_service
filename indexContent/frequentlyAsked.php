<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../css/bootstrap.min.css">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="../css/all.min.css">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="../css/style.css">


  <!-- Linking AOS Library -->
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css">

  <title>Online Mobile Repair Service Center</title>
</head>

<body>

<img class="wave d-print-none" src="../images/wave.png">


<section id="frequentlyAsked" class=" mt-5">
<div class="box">
   <h3 class="heading font-weight-bold">FAQs</h3>
   <div class="faqs">
      <details>
         <summary class="font-weight-bold">THINGS TO PERFORM TO GET STARTED WITH THIS APPLICATION?</summary>
         <p class="text ml-4 mt-2">In order to exercise the application's services, you either need to REGISTER or LOGIN to the application.</p>
      </details>
      <details>
         <summary class="font-weight-bold">HOW DO I SEND IN MY MOBILE DEVICE FOR REPAIR?</summary>
         <p class="text ml-4 mt-2">After logging into the application, the user can have the privilege to SUBMIT the REQUEST FORM, by appropriate filling and submitting this form they can send their issues.</p>
      </details>
      <details>
         <summary class="font-weight-bold">FIND AS THE DEVICE ISSUE GETS ASSIGNED?</summary>
         <p class="text ml-4 mt-2">After you submit the issue to the admin then the admin observes and if the issue is appropriate then gets ASSIGNED.</p>
      </details>
      <details>
         <summary class="font-weight-bold">GET TO KNOW THE STATUS OF THE DEVICE?</summary>
         <p class="text ml-4 mt-2">In the user profile, if CHECK DEVICE STATUS is clicked then only the detail of the issue that are assigned by the admin are only displayed.</p>
      </details>
      <details>
         <summary class="font-weight-bold">HOW TO GET CONNECTED?</summary>
         <p class="text ml-4 mt-2">User can submit the CONTACT FORM to contact and also can place their FEEDBACK regarding the APPLICATION via contact form.</p>
      </details>
      <details>
         <summary class="font-weight-bold">WHAT ABOUT PRIVACY?</summary>
         <p class="text ml-4 mt-2">We clarify you that PRIVACY is our major concern so your privacy is undoubtedly SECURED.</p>
      </details>
   </div>
</div>
</section>

<center>
    <a href="../index.php" class="btn btn-primary text-center" title="Back To Home"><i class="fas fa-backward"></i> Back</a>
</center>

  <!-- Boostrap JavaScript -->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/all.min.js"></script>

  <!-- JS for Mode in a Webpage (Dark or light mode) -->
  <script src="../js/custom.js"></script>
  
</body>

</html>