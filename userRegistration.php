<?php
  include('dbConnection.php');

  if(isset($_POST['rSignup'])){
    $rName = mysqli_real_escape_string($conn,trim($_POST['rName']));
    $rEmail = mysqli_real_escape_string($conn,trim($_POST['rEmail']));
    $rPassword = md5($_POST['rPassword']);
    $cPassword = md5($_POST['cPassword']);

    // Checking for Empty Fields
    if(($_POST['rName'] == "") || ($_POST['rEmail'] == "") || ($_POST['rPassword'] == "") || ($_POST['cPassword'] == "")){
      $regmsg = '<div class="alert alert-warning mt-2" role="alert"> All Fields are Required. </div>';

    // } else if(strlen($rPassword) > 16){
    //   //since in database the character size of password is defined upto 32 characters.
    //   $regmsg = '<div class="alert alert-warning mt-2" role="alert"> Password must be less than 32 characters. </div>';
      
      
    }elseif($rPassword != $cPassword){
      $regmsg = '<div class="alert alert-warning mt-2" role="alert"> Password did not Match. </div>';
    }
    else{
      $sql = "SELECT email FROM userLogin_tb WHERE email='".$_POST['rEmail']."'";
      $result = $conn->query($sql);
      if($result->num_rows == 1){
        $regmsg = '<div class="alert alert-warning mt-2" role="alert"> Email ID Already Registered. </div>';
        header("refresh: 1");
      } else {

        $sql = "INSERT INTO userLogin_tb(name, email, password) VALUES ('$rName','$rEmail', '$rPassword')";
        if($conn->query($sql) == TRUE){
          $regmsg = '<div class="alert alert-success mt-2" role="alert"> Account Created Successfully. </div>';
          // echo "<script> location.href='index.php'; </script>";
          header("refresh: 1");
        } else {
          $regmsg = '<div class="alert alert-danger mt-2" role="alert"> Unable to Create Account </div>';
          echo "<script> location.href='index.php'; </script>";
        }
      }
    }   
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="css/all.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/style.css">

  <title>User Registration</title>
</head>

<body>

  <img class="wave" src="images/wave.png">
	<header class="container mt-5 shadow-lg mb-5 bg-white rounded" id="register">
		<div class="img">
			<img src="images/bg.svg">
		</div>
		<div class="login-content">
    
    <form action="" method="POST">
				<img src="images/avatar.svg">
        <h2 class="title"><span>Create</span> An Account</h2>
           		<div class="input-div one">
           		   <div class="i">
           		   		<i class="fas fa-user"></i>
           		   </div>
           		   <div class="div">
                  <input type="text"
            class="form-control" placeholder="Enter Name" name="rName" value="<?php if(isset($rName)) {echo $rName; } ?>" required>
           		   </div>
           		</div>
               <div class="input-div one">
           		   <div class="i">
                  <i class="far fa-envelope"></i>
           		   </div>
           		   <div class="div">
                  <input type="email"
            class="form-control" placeholder="Enter Email" name="rEmail" value="<?php if(isset($rEmail)) {echo $rEmail; }?>"  required>
           		   </div>
           		</div>

           		<div class="input-div pass">
           		   <div class="i"> 
           		    	<i class="fas fa-lock"></i>
           		   </div>
           		   <div class="div">
                  <input type="password" class="form-control" placeholder="Enter Password" name="rPassword" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
            	   </div>
            	</div>

              <div class="input-div pass">
           		   <div class="i"> 
           		    	<i class="fas fa-lock"></i>
           		   </div>
           		   <div class="div">
                  <input type="password" class="form-control" placeholder="Confirm Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" name="cPassword" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
            	   </div>
            	</div>

              <button type="submit" class="btn" name="rSignup" title="Register Your Account"><i class="fas fa-user-plus"></i> Sign Up</button>
              <?php if(isset($regmsg)) {echo $regmsg; } ?>
              <a href="index.php" class="btn text-center" title="Back"><i class="fas fa-backward"></i> Back
              to Home</a>
              <div class="">Already Have an Account ? <a href="user/userLogin.php" class="text-center"><i class="fas fa-sign-in-alt"></i> Click Here To Login</a></div>
            </form>              
        </div>
</header>

  <!-- Boostrap JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/all.min.js"></script>
</body>

</html>